@extends('template')
@section("content")

    @if(empty($caracteresUsers))
        <p>pas de perso pour le momement à afficher</p>
    @else
        <div >
            <div class="PageTitle-mt pageTitle">
                <h2>Mes personnages</h2>
            </div>
            <div class="allCaracters">
                @foreach($caracteresUsers as $caracteres)
                            <div class="cardCaract" >
                                <div class="cardCaract-content">
                                    <div class="cardCaract-photo">
                                        <img class="cardCaract-img"  src="{{$caracteres->caracters_img_location}}" alt="{{$caracteres->caracters_img_location}}">
                                        <h5 class="text-center">{{$caracteres->caracters_name}}  {{$caracteres->caracters_forname}}</h5>
                                    </div>
                                    <div class="cardCaract-body">
                                        <div class="cardCaract-carac">
                                            <div class="carac">Age: {{$caracteres->caracters_age}}</div>
                                            <div class="traits"></div>
                                            <div class="carac">Genre : {{$caracteres->caracters_sexe}}</div>
                                            <div class="traits"></div>
                                            <div class="carac">Race : {{$caracteres->caracters_races_id}}</div>
                                        </div>

                                        <p class="card-text">Histoire : {{$caracteres->caracters_back_ground}}</p>
                                    </div>
                                </div>

                                <div class="cardCaract-btn">
                                    <a href="{{route('mescaracters.update', $caracteres->caracters_id)}}" class="card-link"><img class="icon" src="{{ URL::asset('img/svg/pencil.svg')}}" alt=""></a>
                                    <a href="{{ route('MesPerso') }}" onclick="event.preventDefault();confirm('Etes-vous vraiment sûr de supprimer ce personnage?')?document.getElementById('delete-form-{{$caracteres->caracters_id}}').submit():event.preventDefault();">
                                        <img class="icon" src="{{ URL::asset('img/svg/delete.svg')}}" alt="">
                                    </a>
                                    <form id="delete-form-{{$caracteres->caracters_id}}"  action="{{route('mescaracters.destroy', $caracteres->caracters_id)}}" method="post">
                                        @csrf @method('DELETE')
                                    </form>

                                </div>
                            </div>
                @endforeach

            </div>



        </div>
    @endif


    <div>
        <h3 class="titleCenter">Crée un nouveau personnage</h3>
        <form enctype="multipart/form-data" method="post" target="{{route("storeCractere")}}" class="container">
            <div class="row">
                @csrf
                <div class="col-6">
                    @error('caracters_name')
                    <div  class="col-md-12 alert alert-danger form-control alert-dismissible fade show" role="alert">
                        <i class="fas fa-exclamation-circle" style="color: red;"></i>
                        {{ $message }}
                        <i class="fas fa-exclamation-circle" style="color: red;"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="fas fa-window-close"></i>
                        </button>
                    </div>
                    @enderror
                    <label for="caracters_name">Nom de votre personnage</label>
                    <input type="text"  class="form-control @error('caracters_name') is-invalid @enderror"  value="{{old("caracters_name")}}" name="caracters_name" id="caracters_name" >
                </div>
                <!---------------------------------------------------------------------->
                <div class="col-6">
                    @error('caracters_forname')
                    <div  class="col-md-12 alert alert-danger form-control alert-dismissible fade show" role="alert">
                        <i class="fas fa-exclamation-circle" style="color: red;"></i>
                        {{ $message }}
                        <i class="fas fa-exclamation-circle" style="color: red;"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="fas fa-window-close"></i>
                        </button>
                    </div>
                    @enderror
                    <label for="caracters_forname">prenom de votre personnage</label>
                    <input type="text"  class="form-control @error('caracters_forname') is-invalid @enderror"  value="{{old("caracters_forname")}}" name="caracters_forname" id="caracters_forname" >
                </div>
                <!---------------------------------------------------------------------->
                <div class="col-6">
                    @error('caracters_age')
                    <div  class="col-md-12 alert alert-danger form-control alert-dismissible fade show" role="alert">
                        <i class="fas fa-exclamation-circle" style="color: red;"></i>
                        {{ $message }}
                        <i class="fas fa-exclamation-circle" style="color: red;"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="fas fa-window-close"></i>
                        </button>
                    </div>
                    @enderror
                    <label for="caracters_age">age de votre personnage</label>
                    <input type="number"  class="form-control @error('caracters_age') is-invalid @enderror"  value="{{old("caracters_age")}}" name="caracters_age" id="caracters_age" >
                </div>
                <!---------------------------------------------------------------------->
                <div class="col-6">
                    @error('caracters_img_location')
                    <div  class="col-md-12 alert alert-danger form-control alert-dismissible fade show" role="alert">
                        <i class="fas fa-exclamation-circle" style="color: red;"></i>
                        {{ $message }}
                        <i class="fas fa-exclamation-circle" style="color: red;"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="fas fa-window-close"></i>
                        </button>
                    </div>
                    @enderror
                    <label for="caracters_img_location">Image de profile de votre personnage(facultatif)</label>
                    <input type="file"  class="form-control @error('caracters_img_location') is-invalid @enderror"  value="{{old("caracters_img_location")}}" name="caracters_img_location" id="caracters_img_location" >
                </div>
                <!---------------------------------------------------------------------->
                <div class="col-6">
                    @error('caracters_sexe')
                    <div  class="col-md-12 alert alert-danger form-control alert-dismissible fade show" role="alert">
                        <i class="fas fa-exclamation-circle" style="color: red;"></i>
                        {{ $message }}
                        <i class="fas fa-exclamation-circle" style="color: red;"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="fas fa-window-close"></i>
                        </button>
                    </div>
                    @enderror
                    <label for="caracters_sexe">le genre de votre personnages</label>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input @error('caracters_sexe') is-invalid @enderror" name="caracters_sexe" type="radio"  onclick="displayGenreText()" id="inlineCheckbox1" value="Masculin">
                        <label class="form-check-label" for="inlineCheckbox1">Masculin</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input @error('caracters_sexe') is-invalid @enderror" name="caracters_sexe" type="radio" onclick="displayGenreText()" id="inlineCheckbox2" value="Féminin">
                        <label class="form-check-label" for="inlineCheckbox2">Féminin</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input @error('caracters_sexe') is-invalid @enderror" name="caracters_sexe"  type="radio" onclick="displayGenreText()" id="nonBinaire">
                        <label class="form-check-label" for="nonBinaire">Autres ?</label>
                    </div>
                    <div style="display :none;" id="nonBinaireText"   class="form-check form-check-inline">
                        <input id="nonBinaireinput"   disabled class="form-check-input @error('caracters_sexe') is-invalid @enderror" name="caracters_sexe" type="text"   value="{{old("caracters_sexe")}}">
                        <label class="form-check-label" for="inlineCheckbox2">précisé ?</label>
                    </div>
                </div>
                <!---------------------------------------------------------------------->
                <div class="col-12">
                    @error('caracters_back_ground')
                    <div  class="col-md-12 alert alert-danger form-control alert-dismissible fade show" role="alert">
                        <i class="fas fa-exclamation-circle" style="color: red;"></i>
                        {{ $message }}
                        <i class="fas fa-exclamation-circle" style="color: red;"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="fas fa-window-close"></i>
                        </button>
                    </div>
                    @enderror
                    <label for="caracters_back_ground">Histoire de votre perssonage : </label>
                    <textarea class="form-control @error('caracters_back_ground') is-invalid @enderror"  name="caracters_back_ground" id="caracters_back_ground" ></textarea>
                </div>
            </div>
            <div class="col-12">
                <!---------------------------------------------------------------------->
                @error('caracters_races_id')
                <div  class="col-md-12 alert alert-danger form-control alert-dismissible fade show" role="alert">
                    <i class="fas fa-exclamation-circle" style="color: red;"></i>
                    {{ $message }}
                    <i class="fas fa-exclamation-circle" style="color: red;"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="fas fa-window-close"></i>
                    </button>
                </div>
                @enderror
                <div class="12">
                    <p class="col-12">Race de votre personnage :</p>
                    @foreach($allRaces as $race)
                        <div class="form-check form-check-inline col-12">

                            <input type="radio"  id="caracters_races_id" name="caracters_races_id" value="{{$race->races_id}}"  class=" form-check-input  @error('caracters_forname') is-invalid @enderror">
                            <label  for="caracters_races_id"><strong>{{$race->label}}</strong> </label>

                        </div>
                        <p> "{{$race->description}}" </p>
                    @endforeach
                </div>
                <input type="submit" value="Créez">
            </div>
        </form>
        <script>
            function displayGenreText() {
                // Get the checkbox
                let checkBox = document.getElementById("nonBinaire");
                // Get the output text
                let text = document.getElementById("nonBinaireText");
                let inputDisabled = document.getElementById("nonBinaireinput");
                // If the checkbox is checked, display the output text
                if (checkBox.checked === true){
                    text.style.display = "block";
                    inputDisabled.removeAttribute('disabled');
                } else {
                    text.style.display = "none";
                    inputDisabled.setAttribute('disabled');
                }
            }

        </script>
    </div>
@endsection
