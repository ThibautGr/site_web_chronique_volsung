@error('profile_photo_path')
<div class="pageTitle">
Modifier votre profil
</div>

    <div  class="col-md-12 alert alert-danger form-control alert-dismissible fade show" role="alert">
        <i class="fas fa-exclamation-circle" style="color: red;"></i>
        {{ $message }}
        <i class="fas fa-exclamation-circle" style="color: red;"></i>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <i class="fas fa-window-close"></i>
        </button>
    </div>
    @enderror
    <div class="photoProfil_Update ">
        <h3 class="text-lg font-medium text-gray-900 px-4 sm:px-0">Photo de profil</h3>
        <div class="shadow overflow-hidden sm:rounded-md pad20">
            <form class="row" enctype="multipart/form-data" method="post" action="{{URL::asset("/user/profile")}}">
                @csrf
                <div class="col-6">
                <input  type="hidden" name="{{$this->user->id}}" value="{{$this->user->id}}">
                    <label for="profile_photo_path"> mettre à jour votre photo de profile</label>
                <input  class="form-control  @error('profile_photo_path') is-invalid @enderror" id="profile_photo_path" type="file" name="profile_photo_path">
                <button class="btnPrimary" type="submit"> changer d'image de profile</button>
                </div>
                <div class="col-6">
                    <p>rendu : </p>
                    <img id="blah"  class="img_profile_user" width="70" height="70"  src="{{URL::asset(Auth::user()->profile_photo_path )}}" alt="your image" />
                </div>
            </form>

            <p>Votre image de profile acutelle</p>
            <img src="{{URL::asset(Auth::user()->profile_photo_path )}}" height="70" width="70" alt="img">
        </div>

    </div>


    <x-jet-form-section submit="updateProfileInformation">
        <x-slot name="title">
            {{ __('Information du profil') }}
        </x-slot>

        <x-slot name="description">
            {{ __('mettre à jour ces informations de profile') }}
        </x-slot>

        <x-slot name="form">
            <!-- Profile Photo -->

            @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())
                <div x-data="{photoName: null, photoPreview: null}" class="col-span-6 sm:col-span-4 bg-darkGrey">
                    <!-- Profile Photo File Input -->
                    <input type="file" class="hidden"
                                wire:model="photo"
                                x-ref="photo"
                                x-on:change="
                                        photoName = $refs.photo.files[0].name;
                                        const reader = new FileReader();
                                        reader.onload = (e) => {
                                            photoPreview = e.target.result;
                                        };
                                        reader.readAsDataURL($refs.photo.files[0]);
                                " />

                    <x-jet-label for="photo" value="{{ __('Photo') }}" />

                    <!-- Current Profile Photo -->
                    <div class="mt-2" x-show="! photoPreview">
                        <img src="{{ $this->user->profile_photo_url }}" alt="{{ $this->user->name }}" class="rounded-full h-20 w-20 object-cover">
                    </div>

                    <!-- New Profile Photo Preview -->
                    <div class="mt-2" x-show="photoPreview">
                        <span class="block rounded-full w-20 h-20"
                            x-bind:style="'background-size: cover; background-repeat: no-repeat; background-position: center center; background-image: url(\'' + photoPreview + '\');'">
                        </span>
                    </div>

                    <x-jet-secondary-button class="mt-2 mr-2" type="button" class="form-group btnPrimary" x-on:click.prevent="$refs.photo.click()">
                        {{ __('Sélectionnez une nouvelle photo de profil') }}
                    </x-jet-secondary-button>

                    @if ($this->user->profile_photo_path)
                        <x-jet-secondary-button type="button" class="form-group btnPrimary" wire:click="deleteProfilePhoto">
                            {{ __('Supprimer la photo') }}
                        </x-jet-secondary-button>
                    @endif

                    <x-jet-input-error for="photo" class="mt-2" />
                </div>
            @endif

        <!--  zone dev -->
        <!-- Name -->
            <div class="col-span-6 sm:col-span-4 ">
                <x-jet-label for="name" value="{{ __('Nom') }}" />
                <x-jet-input id="name" type="text" class="mt-1 block w-full" wire:model.defer="state.name" autocomplete="name" />
                <x-jet-input-error for="name" class="mt-2" />
            </div>

            <!-- Email -->
            <div class="col-span-6 sm:col-span-4">
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" type="email" class="mt-1 block w-full" wire:model.defer="state.email" />
                <x-jet-input-error for="email" class="mt-2" />
            </div>
        </x-slot>

        <x-slot name="actions">
            <x-jet-action-message class="mr-3" on="saved">
                {{ __('Sauvegardé') }}
            </x-jet-action-message>

            <x-jet-button wire:loading.attr="disabled" wire:target="photo" class="btnPrimary">
                {{ __('Sauvegarder') }}
            </x-jet-button>
        </x-slot>
    </x-jet-form-section>


