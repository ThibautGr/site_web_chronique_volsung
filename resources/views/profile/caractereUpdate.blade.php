@extends('template')
@section("content")
    @foreach($targetCara as $cara)

{{--zone de test--}}

      <div class="card col-lg-3" >
          <form action="{{route("mescaracters.storeUpdate")}}"   method="post">
              @csrf
              <img class="card-img-top"  src="{{$cara->caracters_img_location}}" alt="{{$cara->caracters_img_location}}">
              <div class="card-body">
                  <h5 class="card-title text-center"><input type="text" name="caracters_name" value="{{$cara->caracters_name}}"> <input type="text " name="caracters_forname" value="{{ $cara->caracters_forname}}"></h5>
                  <p class="card-text">Histoire :<input type="text" name="caracters_back_ground" value="{{ $cara->caracters_back_ground}}"></p>
              </div>
              <ul class="list-group list-group-flush">
                  <input type="number" class="list-group-item" name="caracters_age" value="{{$cara->caracters_age}}">
                  <input type="hidden" class="list-group-item" name="caracters_id" value="{{$cara->caracters_id}}">
                  <li class="list-group-item">Genre :{{$cara->caracters_sexe}}</li>
                  @foreach($raceLavbel as $label)
                      <li class="list-group-item">race :{{$label->label}}</li>
                  @endforeach
              </ul>
              <div class="card-body">
                  <input type="submit" value="confirmer la mise à jour">
              </div>
          </form>
      </div>
    @endforeach
@endsection
