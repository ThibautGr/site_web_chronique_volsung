<h2 class="post__header">{{$faction->factions_label}}</h2>

        <article class="post">
            <div>
                @if(isset($faction->factions_symbole))
                @else
                <div class="absolute-bg" style="background-image: url({{ URL::asset('img/BG/BGtisserins.png')}});"></div>
                @endif
            <div class="absolute-bg" style="background-image: url({{URL::asset($faction->factions_symbole)}});"></div>
            </div>
            <div class="post__container">
            <div class="post__content">

                {{-- /////////// --}}

                <section id="wrapper">
                    <div class="content">
                    <!-- Tab links -->
                    <div class="tabs">
                        <button class="tablink tablinks{{$faction->factions_id}} active" data-menu="histoire{{$faction->factions_id}}"><p data-title="histoire{{$faction->factions_id}}">Histoire</p></button>
                        <button class="tablink tablinks{{$faction->factions_id}}" data-menu="leader{{$faction->factions_id}}"><p data-title="leader{{$faction->factions_id}}">Leader</p></button>
                        <button class="tablink tablinks{{$faction->factions_id}}" data-menu="commerces{{$faction->factions_id}}"><p data-title="commerces{{$faction->factions_id}}">Instances et commerces</p></button>
                        <button class="tablink tablinks{{$faction->factions_id}}" data-menu="hors-la-loi{{$faction->factions_id}}"><p data-title="hors-la-loi{{$faction->factions_id}}">Hors la loi</p></button>
                        <button class="tablink tablinks{{$faction->factions_id}}" data-menu="titres{{$faction->factions_id}}"><p data-title="titres{{$faction->factions_id}}">Titres sociaux</p></button>
                        <button class="tablink tablinks{{$faction->factions_id}}" data-menu="conseil{{$faction->factions_id}}"><p data-title="conseil{{$faction->factions_id}}">Conseil du Village</p></button>
                        <button class="tablink tablinks{{$faction->factions_id}}" data-menu="maisnie{{$faction->factions_id}}"><p data-title="maisnie{{$faction->factions_id}}">Maisnie et Vassalite</p></button>
                        <button class="tablink tablinks{{$faction->factions_id}}" data-menu="allegeance{{$faction->factions_id}}"><p data-title="allegeance{{$faction->factions_id}}">Allégeance</p></button>
                        <button class="tablink tablinks{{$faction->factions_id}}" data-menu="astuces{{$faction->factions_id}}"><p data-title="astuces{{$faction->factions_id}}">Astuces</p></button>

                    </div>

                    <!-- Tab content -->
                    <div class="wrapper_tabcontent">
                        <div id="histoire{{$faction->factions_id}}" class="tabcontent tabcontent{{$faction->factions_id}} active">
                            <h3>Histoire</h3>
                            <p>{{$faction->factions_description}}</p>
                        </div>

                        <div id="leader{{$faction->factions_id}}" class="tabcontent tabcontent{{$faction->factions_id}}">
                            <h3>Leader</h3>
                            <p>Bourgmestre bla bla bla</p>
                        </div>

                        <div id="commerces{{$faction->factions_id}}" class="tabcontent tabcontent{{$faction->factions_id}}">
                            <h3>Instances et commerces</h3>
                            <p>{{$faction->factions_description}}</p>
                        </div>

                        <div id="hors-la-loi{{$faction->factions_id}}" class="tabcontent tabcontent{{$faction->factions_id}}">
                            <h3>Hors la loi</h3>
                            <p>{{$faction->factions_description}}</p>
                        </div>

                        <div id="titres{{$faction->factions_id}}" class="tabcontent tabcontent{{$faction->factions_id}}">
                            <h3>Titres sociaux</h3>
                            <p>{{$faction->factions_description}}</p>
                        </div>
                        <div id="conseil{{$faction->factions_id}}" class="tabcontent tabcontent{{$faction->factions_id}}">
                            <h3>Conseil du Village</h3>
                            <p>{{$faction->factions_description}}</p>
                        </div>
                        <div id="maisnie{{$faction->factions_id}}" class="tabcontent tabcontent{{$faction->factions_id}}">
                            <h3>Maisnie et Vassalite</h3>
                            <p>{{$faction->factions_description}}</p>
                        </div>
                        <div id="allegeance{{$faction->factions_id}}" class="tabcontent tabcontent{{$faction->factions_id}}">
                            <h3>Allégeance</h3>
                            <p>{{$faction->factions_description}}</p>
                        </div>
                        <div id="astuces{{$faction->factions_id}}" class="tabcontent tabcontent{{$faction->factions_id}}">
                            <h3>Astuces</h3>
                            <p>{{$faction->factions_description}}</p>
                        </div>

                    </div>
                    </div>
                </section>

                {{-- //////////// --}}

            </div>
            <div class="post__link">
                <a href="{{route('allFaction.updater',$faction->factions_id)}}">mettre a jour</a>
            </div>
            </div>
        </article>



