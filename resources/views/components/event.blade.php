<div class="eventContainer">
    <div class="eventDetail">
        <div class="eventDates">
            <div class="eventStart">{{$event->events_start}}</div>
            <div class="eventEnd">{{$event->events_end}}</div>
        </div>
        <div class="eventContent">
            <div class="eventTitle">{{$event->events_label}}</div>
            <div class="eventDescription">{{$event->events_description}}</div>
        </div>
    </div>
    <div class="eventPriceSection">
        <h3>Réservez votre billet maintenant</h3>
        <div class="eventPrice">Prix du billet : <a href="/" class="btnPrimary">Billet 3 jours à {{$event->events_price}}€</a> </div>
    </div>
</div>
