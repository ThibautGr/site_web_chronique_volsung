<div class="loginPage">
    <x-guest-layout>
        <div class="loginPageContent">
        <x-jet-authentication-card>
            <x-slot name="logo">
                <x-jet-authentication-card-logo />
            </x-slot>

            <x-jet-validation-errors class="mb-4" />

            @if (session('status'))
                <div class="mb-4 font-medium text-sm text-green-600">
                    {{ session('status') }}
                </div>
            @endif

            <form method="POST" action="{{ route('login') }}" class="formLogin">
                <h2>Connexion</h2>
                <p class="text-muted">Veuillez rentrer vos identifiants de connexion</p>
                @csrf

                <div class="flexCol">
                    <x-jet-label for="email" value="{{ __('Email') }}" />
                    <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
                </div>

                <div class="flexCol">
                    <x-jet-label for="password" value="{{ __('Mot de passe') }}" />
                    <x-jet-input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="current-password" />
                </div>

                <div class="flexCol">
                    <label for="remember_me" class="flex items-center">
                        <input id="remember_me" type="checkbox" class="form-checkbox" name="remember">
                        <span class="ml-2 text-sm text-gray-600">{{ __('Se souvenir de moi') }}</span>
                    </label>
                </div>

                <div class="flexCol">

                    <x-jet-button class="btnPrimary">
                        {{ __("S'identifier") }}
                    </x-jet-button>
                </div>
                <a class="text-muted" href="{{"inscription"}}"> Pas encore inscrit ?</a>
                @if (Route::has('password.request'))
                    <a class="text-muted form-text" href="{{ route('password.request') }}">
                        {{ __('Mot de passe oublié ?') }}
                    </a>
                @endif
            </form>
        </x-jet-authentication-card>
    </div>
    </x-guest-layout>

</div>
