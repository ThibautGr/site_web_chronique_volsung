<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <link rel="icon" type="image/png" href="{{ URL::asset('img/logo/Tisserins_de_Sanctuaires.png') }}" />
    <meta charset="UTF-8">
    <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Les Tisserins du Sanctuaire</title>
    <!--font--->
    <!--font--->
    <!--Styles-->

    <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.7.0/dist/alpine.js" defer></script>

    <!--Styles-->
</head>

<body>
    <nav class="navbar bg-transparent navbar-fixed-top">

        <a href="{{ url('/') }}" class="navbar-brand">
            <img width="100" class="logo-navbar align-top" src="{{ URL::asset('img/logo/logo_asso.png') }}">
        </a>


        <a class="navbar-brand" href="{{ route('association') }}"> Les Tisserins du Sanctuaire</a>
        <a class="navbar-brand" href="{{ route('wiki') }}">Wiki de volsung</a>
        <div class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                Grandeur Nature
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ route('events') }}">Prochain GN</a>
                <a class="dropdown-item" href="#">Ancien GN</a>
            </div>
        </div>
        @auth

            <div class="btn-group">
                {{-- <button type="button" class="btn-login btnDore">{{ Auth::user()->forname }}</button> --}}
                <button type="button" class="btn-login btnDore  dropdown-toggle dropdown-toggle-split"
                    id="dropdownMenuReference" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                    data-reference="parent">
                    <img class="img_profile_user" width="60" src="{{ URL::asset(Auth::user()->profile_photo_path) }}">
                </button>
                <div class="dropdown-menu dropdown-menu-user marRight40" aria-labelledby="dropdownMenuReference">
                    <!-- Account Management -->
                    <x-jet-responsive-nav-link class="dropdown-item" href="{{ route('profile.show') }}"
                        :active="request()->routeIs('profile.show')">
                        {{ __('Profile') }}
                    </x-jet-responsive-nav-link>

                    @if (Laravel\Jetstream\Jetstream::hasApiFeatures())
                        <x-jet-responsive-nav-link href="{{ route('api-tokens.index') }}"
                            :active="request()->routeIs('api-tokens.index')">
                            {{ __('API Tokens') }}
                        </x-jet-responsive-nav-link>
                    @endif
                    <a class="dropdown-item" href="{{ route('MesPerso') }}">Mes personnages</a>
                    @if (Auth::user()->admin == true)
                        <a class="dropdown-item" href="{{ route('HomeAdmin') }}"> Zone orga</a>
                        <a class="dropdown-item" href="{{ route('allUser') }}"> Tous les membres</a>
                        @if (Auth::user()->admin == true && Auth::user()->users_id_level_admin === 1)
                            <p class="navbar-brand">Action webmaster orga : </p>
                            <a class="navbar-brand" href="{{ route('createEventsAdmin') }}">Rajouter un event</a>
                            <a class="navbar-brand">Confirmer une inscription</a>
                            <a class="navbar-brand" href="{{ route('createRacesAdmin') }}">Les races</a>
                            <a class="navbar-brand">Les classes</a>
                            <a class="navbar-brand" href="{{ route('allFaction') }}">Les Factions</a>
                            <a class="navbar-brand" href="{{ route('allCompagnies') }}">Les compagnies</a>
                            <a class="navbar-brand" href="{{ route('allGrades') }}">Les Grades</a>
                            <a class="navbar-brand" href="{{ route('allCharacters') }}">listes des personnages</a>
                        @endif
                    @endif
                    <div class="dropdown-divider"></div>
                    <form method="POST" action="{{ route('logout') }}">
                        @csrf
                        <x-jet-dropdown-link class="dropdown-item btnTransp" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                                        this.closest('form').submit();">
                            {{ __('déconnexion') }}
                        </x-jet-dropdown-link>
                    </form>
                </div>
            </div>
        @endauth
        @guest
            <nav>
                <a class="navbar-brand" href="{{ url('inscription') }}">inscription</a>
                <!--------------------Modal connexion--------------------------->
                <!-- Button trigger modal -->
                <a href="{{ url('login') }}"> connexion</a>

            </nav>
        @endguest

    </nav>
    <script>
        window.onscroll = function() {
            let navbar = document.querySelector(".navbar")
            let top = window.scrollY
            if (top >= 100) {
                navbar.classList.add("scrolled")
            } else {
                navbar.classList.remove("scrolled")
            }
        }

        if (window.location.pathname != "/") {
            let navbar = document.querySelector(".navbar")
            navbar.classList.remove("bg-transparent")
            navbar.classList.add("bg-black")
        }
        console.log(window.location.pathname)

    </script>

</body>

</html>
