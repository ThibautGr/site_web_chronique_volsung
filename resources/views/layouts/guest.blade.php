@extends('template')

@section("content")
    <body>
        <div >
            {{ $slot }}
        </div>
    </body>
</html>
@endsection
