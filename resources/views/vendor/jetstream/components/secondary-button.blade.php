<button {{ $attributes->merge(['type' => 'button', 'class' => 'btn, btn-login']) }}>
    {{ $slot }}
</button>
