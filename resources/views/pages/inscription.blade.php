@extends('template')

<div class="registerPage">

@section("content")


        <form method="POST" class="container" action="{{ route('inscription.create')}}" enctype="multipart/form-data">

            <div class="registerPageContent">
            <h2>Inscription</h2>
            <p class="text-muted">Veuillez remplir tous les champs obligatoire avec un <span class="redAstx">*</span></p>
            @csrf
            <!--Name and forname-->
            <div class="row">
                @include('components.flashAlert')
                @error('name')
                <div  class="col-md-12 alert alert-danger form-control alert-dismissible fade show" role="alert">
                    <i class="fas fa-exclamation-circle" style="color: red;"></i>
                    {{ $message }}
                    <i class="fas fa-exclamation-circle" style="color: red;"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="fas fa-window-close"></i>
                    </button>
                </div>
                @enderror
                <div class="col">
                    <label for="name">Nom<span class="redAstx">*</span></label>
                    <input  value="{{ old('name') }}" type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name">
                </div>

                @error('forname')
                <div  class="col-md-12 alert alert-danger form-control alert-dismissible fade show" role="alert">
                    <i class="fas fa-exclamation-circle" style="color: red;"></i>
                    {{ $message }}
                    <i class="fas fa-exclamation-circle" style="color: red;"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="fas fa-window-close"></i>
                    </button>
                </div>
                @enderror
                <div class="col">
                    <label for="forname">Prénom<span class="redAstx">*</span></label>
                    <input type="text"  class="form-control @error('forname') is-invalid @enderror" value="{{old('forname')}}" id="forname" name="forname">
                </div>
            </div>
                <!--Name and forname-->
                <!--mail and age--->
            <div class="row">

                @error('email')
                <div  class="col-md-12 alert alert-danger form-control alert-dismissible fade show" role="alert">
                    <i class="fas fa-exclamation-circle" style="color: red;"></i>
                    {{ $message }}
                    <i class="fas fa-exclamation-circle" style="color: red;"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="fas fa-window-close"></i>
                    </button>
                </div>
                @enderror
                <div class="col">
                    <label for="email">Adresse mail<span class="redAstx">*</span></label>
                    <input value="{{old('email')}}" type="text" class="form-control @error('forname') is-invalid @enderror" name="email" id="email">
                </div>

                @error('age')
                <div  class="col-md-12 alert alert-danger form-control alert-dismissible fade show" role="alert">
                    <i class="fas fa-exclamation-circle" style="color: red;"></i>
                    {{ $message }}
                    <i class="fas fa-exclamation-circle" style="color: red;"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="fas fa-window-close"></i>
                    </button>
                </div>
                @enderror
                <div class="col">
                    <label for="age">Votre age<span class="redAstx">*</span></label>
                    <input value="{{old('age')}}" type="number" class="form-control @error('age') is-invalid @enderror"  name="age" id="age">
                </div>
            </div>
                <!--mail and age--->
                <!--pwd--->
            <div class="row">
                @error('password')
                <div  class="col-md-12 alert alert-danger form-control alert-dismissible fade show" role="alert">
                    <i class="fas fa-exclamation-circle" style="color: red;"></i>
                    {{ $message }}
                    <i class="fas fa-exclamation-circle" style="color: red;"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="fas fa-window-close"></i>
                    </button>
                </div>
                @enderror
                <div class="col">
                    <label for="password">Mot de passe<span class="redAstx">*</span></label>
                    <input type="password" value="{{old('password')}}" class="form-control @error('password') is-invalid @enderror"  name="password" id="password">
                </div>

                <div class="col">
                    <label for="password_conf">Confirmation du mots de passe<span class="redAstx">*</span></label>
                    <input type="password" class="form-control" name="password_confirmation" id="password_conf">
                </div>
            </div>
                <!--pwd--->
            <div class="form-group row">
                @error('profile_photo_path')
                <div  class="col-md-12 alert alert-danger form-control alert-dismissible fade show" role="alert">
                    <i class="fas fa-exclamation-circle" style="color: red;"></i>
                    {{ $message }}
                    <i class="fas fa-exclamation-circle" style="color: red;"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="fas fa-window-close"></i>
                    </button>
                </div>
                @enderror
                <div class="col-6">
                    <label for="profile_photo_path">Votre image de profile</label>
                    <input type='file' id="profile_photo_path" class="form-control @error('profile_photo_path') is-invalid @enderror" name="profile_photo_path" />
                </div>
                <div class="col-6">
                    <p>rendu : </p>
                    <img id="blah"  class="img_profile_user" width="70" height="70"  src="{{URL::asset("img/imgIconeUser/user_type_un.jpg")}}" alt="your image" />
                </div>
            </div>

                <!--Zone information facultative--->
            <div class="flexCol">
                <button type="submit" class="inline-flex items-center px-4 py-2 bg-gray-800 btnPrimary">S'inscrire !</button>
            </div>



            <div class="text-muted"><p>Toutes les informations données à l'association "Les Tisserins du Sanctuaire" ne seront divulguées à personne d'autre que l'association "Les Tisserins du Sanctuaire"</p></div>
        </div>
        </form>

</div>
@endsection
