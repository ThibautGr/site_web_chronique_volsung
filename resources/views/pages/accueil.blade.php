@extends('template')

@section("content")
<link rel="stylesheet" href="{{URL::asset("css/app.css")}}">
<div class="homePage">


    <div class="homeTitle">
        <a href="/">
            <h1>Les Tisserins du Sanctuaire</h1>
            <h2>Grandeur Nature : Les Chroniques de Volsung</h2>
        </a>
    </div>
    <img class="arrownDown" src="{{URL::asset("img/svg/arrow.svg")}}" alt="goDownArrow">


</div>

<div class="splitScreen"></div>

<div class="discoverUnivers">
    <h2 class="discoverUnivers_title titlesHomepage">Découvrez l'univers des Chroniques de Volsung</h2>
    <div class="discoverUnivers_content">
       <p> Du néant naquit Kral. <br>
        Et de ses mains naquirent les Wrydalahr, les seigneurs de
        l’univers. Depuis lors, Dozkeyn, la grande horloge, est le
        théâtre d’événements sans précédents. Dans le firmament
        s’élevèrent Gu, la Lune, et Krein, le Soleil. Et au coeur
        de ces deux astres, les premiers dieux créèrent Volsung, la
        Terre des Âges.<br>
        Durant des millénaires, ce monde resta froid et sombre. Mais
        les dieux et leurs enfants façonnèrent le monde et les premières
        créatures apparurent. Virent par la suite les Oldalahr, les
        Héritiers des Dieux.<br>
        Ces êtres mortels évoluèrent et se mirent à la découverte de
        Volsung. De grands empires apparurent, des civilisations se
        fondèrent, des dynasties s’illuminèrent de mille feux sous la
        protection des dieux. Mais rien n’est éternel.<br>
        S’en suivirent à l’âge glorieux des dieux, de nombreux âges,
        ponctués de guerres et de conquêtes et les ténèbres vinrent
        s’installer. Les conflits depuis toujours ne cessent et la loi du
        plus fort ou du plus malin est de mise.<br>
        Entre fureur divine et volonté ardente des mortels, la collision
        ne fait que commencer. Des heures sombres s’annoncent
        désormais. Les conflits font rage au coeur du monde de
        Volsung.</p>

        <button class="btnPrimary">En savoir plus sur l'univers</button>
    </div>

</div>

<div class="splitScreen"></div>

<div class="joinUs">
    <h2 class="joinUs_title titlesHomepage">Rejoignez l'aventure </h2>
    <div class="joinUs_content">
        <p>Creez votre personnage et entrer dans l'univers des Chroniques de Volsung pour le prochain grandeur nature !</p>
        <div class="joinUs_content-carac btnPrimary">
            Création et personnalisation de votre personnage
        </div><br>
        <p>Préparez votre équipement et vos bagages en prenant en compte nos conseils et réglementations !</p>
        <div class="joinUs_content-regle btnPrimary">
            Se préparer pour le grandeur nature
        </div>
    </div>
</div>

<div class="splitScreen"></div>

<div class="assoResume">

</div>

@endsection


