@extends('template')

@section("content")

<div class="assoPage">

    <div class="container">
        <!--zone titre de la page-->
        <div class="pageTitle">
            <h1 class="text-center">Présentation de l'association les Tisserins du Sanctuaire</h1>
        </div>
        <!--zone contenu de la page-->
        <div class="content">
            <!--zone description-->
            <div class="row ">
                <div class="row-cols-12">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum semper velit lorem, et dapibus massa blandit at. Sed dictum leo odio, vitae tincidunt quam condimentum ut. Donec a lectus vitae velit interdum ultrices ac sagittis libero. Quisque hendrerit condimentum tellus. Vivamus non urna posuere, scelerisque diam id, ultrices elit. Sed id dolor orci. Proin eu odio quis tellus porttitor maximus. Etiam at risus consectetur odio malesuada egestas. Cras vel lorem sed quam consectetur molestie eget vel neque.</p>
                    <p>Aenean lacinia, ipsum nec dictum rhoncus, urna velit laoreet massa, sit amet pretium leo erat in lectus. Nullam aliquam nec nunc nec varius. Vivamus dictum dui in sem cursus congue. Praesent at mollis tortor. Etiam faucibus dui felis, eu scelerisque elit fermentum eget. Quisque euismod quam sed lorem tincidunt congue. Quisque rhoncus neque sed posuere tincidunt. Phasellus eros nisi, accumsan vitae venenatis in, pretium sed quam. Aliquam vulputate mi sodales magna ultrices, ornare porta ex tempor. Nam hendrerit nulla sed velit consequat finibus. Maecenas sit amet aliquam ante. Maecenas lacinia, dui sit amet iaculis posuere, sapien lacus bibendum justo, ac malesuada tortor augue sit amet magna. Mauris convallis a sapien quis placerat.</p>
                </div>
            </div>
            <!--zone membre asso
            TODO: une fois rentré en BDD, faire un foreach et créer un component qu'on pourra appeler et remplir pour chaque membre de l'asso
            -->

            <div class="row text-center membersCard">
                <div class="card bg-img col-lg-3" style="width: 18rem;">
                    <div class="card-header"><h5 class="card-title">nom du membre de l'assos</h5></div>
                    <div class="card-body">
                        <img class="card-img-top" height="50%"  src="{{URL::asset("img/parchemin/poule.jpg")}}" alt="Card image cap">
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btnPrimary">Go somewhere</a>
                    </div>
                </div>
                <div class="card bg-img col-lg-3" style="width: 18rem;">
                    <div class="card-header"><h5 class="card-title">nom du membre de l'assos</h5></div>
                    <div class="card-body">
                        <img class="card-img-top" height="50%"  src="{{URL::asset("img/parchemin/poule.jpg")}}" alt="Card image cap">
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btnPrimary">Go somewhere</a>
                    </div>
                </div>
                <div class="card bg-img col-lg-3" style="width: 18rem;">
                    <div class="card-header"><h5 class="card-title">nom du membre de l'assos</h5></div>
                    <div class="card-body">
                        <img class="card-img-top" height="50%"  src="{{URL::asset("img/parchemin/poule.jpg")}}" alt="Card image cap">
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btnPrimary">Go somewhere</a>
                    </div>
                </div>
                <div class="card bg-img col-lg-3"  style="width: 18rem;">
                    <div class="card-header"><h5 class="card-title">nom du membre de l'assos</h5></div>
                    <div class="card-body">
                        <img class="card-img-top" height="50%"  src="{{URL::asset("img/parchemin/poule.jpg")}}" alt="Card image cap">
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btnPrimary">Go somewhere</a>
                    </div>
                </div>
                <div class="card bg-img col-lg-3" style="width: 18rem;">
                    <div class="card-header"><h5 class="card-title">nom du membre de l'assos</h5></div>
                    <div class="card-body">
                        <img class="card-img-top" height="50%"  src="{{URL::asset("img/parchemin/poule.jpg")}}" alt="Card image cap">
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btnPrimary">Go somewhere</a>
                    </div>
                </div>
                <div class="card bg-img col-lg-3" style="width: 18rem;">
                    <div class="card-header"><h5 class="card-title">nom du membre de l'assos</h5></div>
                    <div class="card-body">
                        <img class="card-img-top" height="50%"  src="{{URL::asset("img/parchemin/poule.jpg")}}" alt="Card image cap">
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btnPrimary">Go somewhere</a>
                    </div>
                </div>
                <div class="card bg-img col-lg-3" style="width: 18rem;">
                    <div class="card-header"><h5 class="card-title">nom du membre de l'assos</h5></div>
                    <div class="card-body">
                        <img class="card-img-top" height="50%"  src="{{URL::asset("img/parchemin/poule.jpg")}}" alt="Card image cap">
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btnPrimary">Go somewhere</a>
                    </div>
                </div>
                <div class="card bg-img col-lg-3"  style="width: 18rem;">
                    <div class="card-header"><h5 class="card-title">nom du membre de l'assos</h5></div>
                    <div class="card-body">
                        <img class="card-img-top" height="50%"  src="{{URL::asset("img/parchemin/poule.jpg")}}" alt="Card image cap">
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btnPrimary">Go somewhere</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
