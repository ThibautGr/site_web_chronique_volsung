@extends('template')
@section("content")
<section class="app">
    <aside class="sidebar">
           <header>
          <h2>Wiki de Volsung</h2>
        </header>
      <nav class="sidebar-nav">

        <ul>
          <li>
            <a href="#"><img class="icon" src="{{ URL::asset('img/svg/scroll.svg')}}" alt=""> <span>Les règlementation du GN</span></a>
            <ul class="nav-flyout">
              <li>
                <a href="#">La création de personnage</a>
              </li>
              <li>
                <a href="#">Les métiers</a>
              </li>
              <li>
                <a href="#">Mise en scène</a>
              </li>
              <li>
                <a href="#">Armes et armures</a>
              </li>
              <li>
                <a href="#"></i>La carte</a>
              </li>
            </ul>
          </li>
          <li>
            <a href="#"><img class="icon" src="{{ URL::asset('img/svg/heraldic.svg')}}" alt=""><span class="">Les factions</span></a>
            <ul class="nav-flyout">
                @foreach($allContent[0] as $faction)
                <li>
                    <a href="#">{{$faction->factions_label}}</a>
                </li>
                @endforeach
            </ul>
          </li>
          <li>
            <a href="#"><img class="icon" src="{{ URL::asset('img/svg/wizard.svg')}}" alt=""><span class="">Les classes</span></a>
            <ul class="nav-flyout">
                @foreach($allContent[1] as $classe)
                <li>
                    <a href="#">{{$classe->label}}</a>
                </li>
                @endforeach
            </ul>
          </li>
          <li>
            <a href="#"><img class="icon" src="{{ URL::asset('img/svg/troll.svg')}}" alt=""><span class="">Les races</span></a>
            <ul class="nav-flyout">
                @foreach($allContent[2] as $race)
                <li>
                    <a href="#">{{$race->label}}</a>
                </li>
                @endforeach
            </ul>
          </li>
          <li>
            <a href="#"><img class="icon" src="{{ URL::asset('img/svg/castle.svg')}}" alt=""><span class="">Les compagnies</span></a>
            <ul class="nav-flyout">
                @foreach($allContent[3] as $compagnie)
                <li>
                    <a href="#">{{$compagnie->label}}</a>
                </li>
                @endforeach
            </ul>
          </li>
      </nav>
    </aside>
    <div class="contentWiki">

    </div>
  </section>
  @endsection
