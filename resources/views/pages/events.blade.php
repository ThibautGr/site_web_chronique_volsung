@extends('template')

@section("content")

    <div class="PageTitle-mt pageTitle">
        <h1 class="text-center">Prochain GN : Les chroniques de Volsung</h1>
    </div>

    @foreach($allEvents as $event)
        <x-event :event=$event/>
    @endforeach


@endsection
