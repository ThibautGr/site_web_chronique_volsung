@extends('template')

@section("content")

<div class="PageTitle-mt pageTitle">
    <h2>Créer une race</h2>
</div>

    <div class="allContent">
        <h4>Liste des races déja existante : </h4>
        <ul>
            @foreach($allraces as $race)
            <li>{{$race->label}}</li>
            @endforeach
        </ul>
    </div>
<div class="formAdmin">
    <form target="{{route("stroreRace")}}" method="post" class="container">
        <h3 class="titleCenter">Créer une race</h3>
        @csrf
        <div class="formSection">
            @error('label')
            <div  class="col-md-12 alert alert-danger form-control alert-dismissible fade show" role="alert">
                <i class="fas fa-exclamation-circle" style="color: red;"></i>
                {{ $message }}
                <i class="fas fa-exclamation-circle" style="color: red;"></i>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <i class="fas fa-window-close"></i>
                </button>
            </div>
            @enderror
            <label for="label">Nom de de la race que vous voulez rajoutez</label>
            <input type="text"  class="form-control @error('label') is-invalid @enderror"  value="{{old("label")}}" name="label" id="label" >
        </div>

        <div class="formSection">
            @error('logevity')
            <div  class="col-md-12 alert alert-danger form-control alert-dismissible fade show" role="alert">
                <i class="fas fa-exclamation-circle" style="color: red;"></i>
                {{ $message }}
                <i class="fas fa-exclamation-circle" style="color: red;"></i>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <i class="fas fa-window-close"></i>
                </button>
            </div>
            @enderror
            <label for="logevity">Logévité de la race que vous allez ajouter</label>
            <input type="number"  class="form-control @error('logevity') is-invalid @enderror"  value="{{old("logevity")}}" name="logevity" id="logevity" >
        </div>

        <div class="formSection">
            @error('description')
            <div  class="col-md-12 alert alert-danger form-control alert-dismissible fade show" role="alert">
                <i class="fas fa-exclamation-circle" style="color: red;"></i>
                {{ $message }}
                <i class="fas fa-exclamation-circle" style="color: red;"></i>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <i class="fas fa-window-close"></i>
                </button>
            </div>
            @enderror
            <label for="description">Histoire de votre personnage : </label>
            <textarea class="form-control @error('description') is-invalid @enderror"  name="description" id="description" ></textarea>
        </div>

        <button class="btnPrimary" type="submit">Ajouter une race</button>

    </form>
</div>
@endsection
