@extends('template')
@section("content")
    <div class="PageTitle-mt pageTitle">
        <h2>Tous les Personnages</h2>
    </div>
    <div class="list-group">
        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">propriétaire</th>
                <th scope="col">id Personnage</th>
                <th scope="col">Nom</th>
                <th scope="col">Prenom</th>
                <th scope="col">Grade</th>

            </tr>
            </thead>
            @foreach($owners as $owner)
                //caracters_users_id
                @foreach($caractereGraded as $Character)
                    @if($owner->id == $Character->caracters_users_id)
                        <tr>
                            <td>{{$owner->name}} {{$owner->forname}}</td>
                            <td scope="row">{{$Character->caracters_id}}</td>
                            <td>{{$Character->caracters_name}}</td>
                            <td>{{$Character->caracters_forname}}</td>
                            <td>{{$Character->label}}</td>
                            <td>0</td>
                        </tr>
                    @endif
                @endforeach
            @endforeach

            <thead>
            <tr>
                <th scope="col">propriétaire</th>
                <th scope="col">id Personnage</th>
                <th scope="col">Nom</th>
                <th scope="col">Prenom</th>
                <th scope="col">Grade</th>
                <th scope="col">information</th>
            </tr>
            </thead>
            @foreach($owners as $owner)
                //caracters_users_id
                @foreach($nonGradedCracter as $Character)
                    @if($owner->id == $Character->caracters_users_id)
                        <tr>
                            <td>{{$owner->name}} {{$owner->forname}}</td>
                            <td scope="row">{{$Character->caracters_id}}</td>
                            <td>{{$Character->caracters_name}}</td>
                            <td>{{$Character->caracters_forname}}</td>
                            <td>non-grader</td>
                            <td>0</td>
                        </tr>
                    @endif
                @endforeach
            @endforeach
        </table>
    </div>
@endsection
