@extends('template')
@section("content")

<div class="PageTitle-mt pageTitle">
    <h2>Créer un événement</h2>
</div>

    <div class="formAdmin">
        <h3 class="titleCenter">Crée un nouvel événement</h3>
        <form enctype="multipart/form-data" method="post" target="{{route("storeEvent")}}" class="container">

                @csrf
                <div class="formSection">
                    @error('events_label')
                    <div  class="col-md-12 alert alert-danger form-control alert-dismissible fade show" role="alert">
                        <i class="fas fa-exclamation-circle" style="color: red;"></i>
                        {{ $message }}
                        <i class="fas fa-exclamation-circle" style="color: red;"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="fas fa-window-close"></i>
                        </button>
                    </div>
                    @enderror
                    <label for="events_label">Nom de l'événement</label>
                    <input type="text"  class="form-control @error('events_label') is-invalid @enderror"  value="{{old("events_label")}}" name="events_label" id="events_label" >
                </div>
                <!---------------------------------------------------------------------->

                <div class="formSection">
                    @error('events_description')
                    <div  class="col-md-12 alert alert-danger form-control alert-dismissible fade show" role="alert">
                        <i class="fas fa-exclamation-circle" style="color: red;"></i>
                        {{ $message }}
                        <i class="fas fa-exclamation-circle" style="color: red;"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="fas fa-window-close"></i>
                        </button>
                    </div>
                    @enderror
                    <label for="events_description">Description de l'événement</label>
                    <textarea class="form-control @error('events_description') is-invalid @enderror"  name="events_description" id="events_description" ></textarea>
                </div>
                <div class="formSection">
                    @error('events_price')
                    <div  class="col-md-12 alert alert-danger form-control alert-dismissible fade show" role="alert">
                        <i class="fas fa-exclamation-circle" style="color: red;"></i>
                        {{ $message }}
                        <i class="fas fa-exclamation-circle" style="color: red;"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="fas fa-window-close"></i>
                        </button>
                    </div>
                    @enderror
                    <label for="events_price">Prix de l'événement</label>
                    <input type="text"  class="form-control @error('events_price') is-invalid @enderror"  value="{{old("events_price")}}" name="events_price" id="events_price" >
                </div>
                <div class="formSection">
                    @error('events_start')
                    <div  class="col-md-12 alert alert-danger form-control alert-dismissible fade show" role="alert">
                        <i class="fas fa-exclamation-circle" style="color: red;"></i>
                        {{ $message }}
                        <i class="fas fa-exclamation-circle" style="color: red;"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="fas fa-window-close"></i>
                        </button>
                    </div>
                    @enderror
                    <label for="events_price">Commencement de l'événement</label>
                    <input type="text"  class="form-control @error('events_start') is-invalid @enderror"  value="{{old("events_start")}}" name="events_start" id="events_start" placeholder="YYYY/MM/DD">
                </div>
                <div class="formSection">
                    @error('events_end')
                    <div  class="col-md-12 alert alert-danger form-control alert-dismissible fade show" role="alert">
                        <i class="fas fa-exclamation-circle" style="color: red;"></i>
                        {{ $message }}
                        <i class="fas fa-exclamation-circle" style="color: red;"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="fas fa-window-close"></i>
                        </button>
                    </div>
                    @enderror
                    <label for="events_price">Fin de l'événement de l'événement</label>
                    <input type="text"  class="form-control @error('events_end') is-invalid @enderror"  value="{{old("events_end")}}" name="events_end" id="events_end" placeholder="YYYY/MM/DD">
                </div>
            <!---------------------------------------------------------------------->
            <button class="btnPrimary" type="submit">Créer l'événement</button>
        </form>

    </div>
@endsection
