@extends('template')
@section("content")
    @foreach($factionToUpdate as $faction)
    <div class="formAdmin">
        <h4 class="titleCenter">Mettre à jour une faction</h4>
        <form action="{{route("storeFactionUpdate")}}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" value="{{$faction->factions_id}}" name="factions_id">
            <div class="factionName formSection">
                <label for="factionName"><h4>Nom de la faction</h4></label>
                <input type="text" value="{{$faction->factions_label}}" name="factions_label">
            </div>
            <div class="factionSymbole formSection">
                <label for="factionName"><h4>Symbole de la faction</h4></label>
                @if(isset($faction->factions_symbole))
                    <input type="file" name="factions_symbole"  >
                    <p>
                        actuelle :
                    </p>
                    <img src="{{URL::asset($faction->factions_symbole)}}" alt="">
                @else
                    <p>Pas encore de symbole voulez vous en mettre un ?</p>
                    <input type="file" name="factions_symbole" >
                @endif
            </div>

            <div class="factionDescription formSection">
                <label for="factionDescription"><h4>Description de la faction</h4></label>
                <textarea  rows="5" cols="30" name="factions_description"> {{$faction->factions_description}}</textarea>
            </div>
            <button class="btnPrimary" type="submit">mettre à jour</button>
          </form>
    </div>

    @endforeach
@endsection
