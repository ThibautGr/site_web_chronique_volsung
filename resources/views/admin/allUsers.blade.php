@extends('template')
@section("content")
    <div class="PageTitle-mt pageTitle">
        <h2>Tous les utilisateurs</h2>
    </div>
    <div class="list-group">
        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">Orga</th>
                <th scope="col">Nom</th>
                <th scope="col">Prenom</th>
                <th scope="col">Niveau admin</th>
                <th scope="col">Notification</th>
                <th scope="col">information</th>
            </tr>
            </thead>
            <tbody>
                @foreach($allUsers as $user)
                    @if($user->admin == true)
                        <tr>
                            <th scope="row">{{$user->id}}</th>
                            <td>{{$user->name}}</td>
                            <td>{{$user->forname}}</td>
                            <td>
                                @foreach($allAdmin as $admin)
                                    @if($admin->id_level_admin == $user->users_id_level_admin)
                                        {{$admin->labelLevelAdmin}}
                                    @endif
                                @endforeach
                            </td>
                            <td>0</td>
                            <td ><a href="{{route('user',$user->id)}}" class="btnPrimary inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">Acceder</a></td>
                        </tr>
                    @endif
                @endforeach
            <thead>
            <tr>
                <th scope="col">Joueur</th>
                <th scope="col">Nom</th>
                <th scope="col">Prenom</th>
                <th scope="col">Niveau admin</th>
                <th scope="col">Notification</th>
                <th scope="col">information</th>
            </tr>
            </thead>
                @foreach($allUsers as $user)
                    @if($user->admin == False)
                        <tr>
                            <th scope="row">{{$user->id}}</th>
                            <td>{{$user->name}}</td>
                            <td>{{$user->forname}}</td>
                            <td>Joueur</td>
                            <td>0</td>
                            <td ><a class="btnPrimary" href="{{route('user',$user->id)}}" >Acceder</a></td>
                        </tr>
                     @endif
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
