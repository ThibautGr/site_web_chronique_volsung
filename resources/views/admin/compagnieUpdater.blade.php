@extends('template')
@section("content")
{{--    compagnieToUpdate--}}
@foreach($compagnieToUpdate as $compagnie)

    <form action="{{route("storeCompagnieUpdate")}}" method="post" enctype="multipart/form-data">
        @csrf

        <input type="hidden" value="{{$compagnie->compagnies_id}}" name="compagnies_id">

        <input type="text" value="{{$compagnie->label}}" name="label">

        @if(isset($compagnie->symboleUrl))
            <input type="file" name="symboleUrl"  >
            <p>
                acutelle :
            </p>
            <img src="{{URL::asset($compagnie->symboleUrl)}}" alt="">
        @else
            <p>pas encore de symbole voulez vous en mettre un  ?</p>
            <input type="file" name="symboleUrl" >
        @endif

        <textarea  rows="5" cols="30" name="description"> {{$compagnie->description}}</textarea>

        <input type="submit" value="mettre à jour">

    </form>

@endforeach
@endsection
