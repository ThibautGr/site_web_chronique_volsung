@extends('template')
@section("content")
<div class="PageTitle-mt pageTitle">
    <h2>Les compagnies</h2>
</div>
    @foreach($allCompagnies as $compagnies)
        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="{{ URL::asset($compagnies->symboleUrl) }}" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title"> {{$compagnies->label}}</h5>
                <p class="card-text">{{$compagnies->description}}</p>
                <a href="{{route('allCompagnies.updater',$compagnies->compagnies_id)}}" class="btn btn-primary">Mettre à jour </a>
            </div>
        </div>
    @endforeach
<div>

    <div class="formAdmin">
        <h3 class="titleCenter">Ajouter une compagnie</h3>
        <form action="{{route('storeNewCompagine')}}" enctype="multipart/form-data" method="post" >
            @csrf

            <div class="formSection">
                <label for="label"><h4>Nom de la compagnie</h4></label>
                <input id="label" type="text" name="label">
            </div>


            <div class="formSection">
                <label for="label"><h4>Description de la compagnie</h4></label>
                <textarea name="description" id="description" cols="10" rows="4"></textarea>
            </div>

            <div class="formSection">
                <label for="symboleUrl"><h4>Symbole de la compagnie</h4></label>
                <input id="symboleUrl" type="file" name="symboleUrl">
            </div>

            <div class="formSection">
                <label for="faction"><h4>Faction</h4></label>
                <select name="compagnies_factions_id" id="">
                    <option value="">--</option>
                @foreach($allFactions as $faction)
                    <option value="{{$faction->factions_id}}">{{$faction->factions_label}}</option>
                @endforeach
                </select>
            </div>


            <button class="btnPrimary" type="submit">Créer la compagnie</button>
        </form>
    </div>
</div>
@endsection
