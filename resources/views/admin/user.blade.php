@extends('template')
@section("content")
    <a href="{{route("allUser")}}" >Retour à la liste </a>
        @foreach($user as $dataU)
            <div class="d-flex justify-content-center "><img class="img_profile_user" width="70" src="{{URL::asset($dataU->profile_photo_path )}}"></div>
            <div class="d-flex justify-content-center"> Nom / prenom : {{$dataU->name}} {{$dataU->forname}}</div>
            <div class="d-flex justify-content-center"></div>
            <div class="d-flex justify-content-center">Mail :{{$dataU->email}}</div>
            <div class="d-flex justify-content-center">age : {{$dataU->age}}</div>
            <div class="d-flex justify-content-center">Admin: @if($dataU->admin === 1) oui @else non @endif
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#Adminupgrade">
                    Changer les droit d'admin pour cette utilisateur
                </button>
            </div>
                <!-- Modal -->
                <div class="modal fade" id="Adminupgrade" tabindex="-1" role="dialog" aria-labelledby="AdminupgradeLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <form action="{{route("updateAdmin")}}" method="POST">
                                @csrf
                                <div class="modal-header">
                                    <h5 class="modal-title" id="AdminupgradeLabel">Modal title</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <input type="hidden" value="{{ $dataU->id}}" name="user_admin_change_id">
                                    cette utilisateur vas passer :
                                    <label for="admin">Admin</label>
                                    <input type="radio" value="1" id="admin" onclick="displayAdminLevel()" name="admin">
                                    <label for="unadmin">Pas admin</label>
                                    <input type="radio" value="0" id="unadmin" onclick="displayAdminLevel()"  name="admin">
                                  <!---liste des admin dispo -->
                                    <div id="adminLabelText" style="display: none">
                                    @foreach($allAdminLabel as $adminLabel)
                                        <label for="{{$adminLabel->labelLevelAdmin}}">{{$adminLabel->labelLevelAdmin}}</label>
                                        <input class="adminLabelinput" id="{{$adminLabel->labelLevelAdmin}}" name="label" value="{{$adminLabel->id_level_admin}}" type="radio">
                                        <br>
                                    @endforeach
                                    </div>
                                </div>
                                <div class="modal-footer">

                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
        @endforeach
        @if(!empty($adminlabel))
            @foreach($adminlabel as $label)
                <div class="d-flex justify-content-center">Niveau admin : {{$label->labelLevelAdmin}}</div>
            @endforeach
        @endif
        <div class="container">
            <div class="row">
                @foreach($caracters as $caracter)
                    @if(empty($caracter))
                        <p>Cette utilisateur n'a pas encore créer de personnage </p>
                    @else
                    <div class="card col-lg-3" >
                        <img class="card-img-top"  src="{{$caracter->caracters_img_location}}" alt="{{$caracter->caracters_img_location}}">
                        <div class="card-body">
                            <h5 class="card-title text-center">{{$caracter->caracters_name}}  {{$caracter->caracters_forname}}</h5>
                            <p class="card-text">Histoire : {{$caracter->caracters_back_ground}}</p>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">age: {{$caracter->caracters_age}}</li>
                            <li class="list-group-item">Genre :{{$caracter->caracters_sexe}}</li>
                            <li class="list-group-item">{{$caracter->caracters_races_id}}</li>
                        </ul>
                        <div class="card-body">
                            <a href="{{route('mescaracters.update', $caracter->caracters_id)}}" class="card-link">Modifier</a>
                        </div>
                        <a class="card-link" href="{{ route('MesPerso') }}" onclick="event.preventDefault();document.getElementById('delete-form-{{$caracter->caracters_id}}').submit();">
                            Supprimer
                        </a>
                        <form id="delete-form-{{$caracter->caracters_id}}" + action="{{route('mescaracters.destroy', $caracter->caracters_id)}}" method="post">
                            @csrf @method('DELETE')
                        </form>
                    </div>
                    @endif
                @endforeach
            </div>
        </div>

        <a href="">modifier</a>


    <script>
        function displayAdminLevel() {
            // Get the checkbox
            let radiotarget = document.getElementById("admin");
            // Get the output text
           // let inputDisabled = document.getElementsByClassName("adminLabelinput");
            let text = document.getElementById("adminLabelText");
            // If the checkbox is checked, display the output text
            if (radiotarget.checked === true){
                text.style.display = "block";
               // inputDisabled.removeNamedItem("disabled");
            } else {
                text.style.display = "none";
               // inputDisabled.setAttribute('disabled', "");
            }
        }
    </script>
@endsection
