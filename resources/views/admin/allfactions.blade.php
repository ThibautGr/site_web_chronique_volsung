
    @extends('template')
        @section("content")

            <div class="PageTitle-mt pageTitle">
                <h2>Les factions</h2>
            </div>

            @foreach($allFactions as $faction)
                <x-oneFaction :faction=$faction/>
            @endforeach

            <script>
                for (let i = 0; i < document.querySelectorAll(".post").length; i++){
                    var tabLinks = document.querySelectorAll(".tablinks" + i);
                    var tabContent = document.querySelectorAll(".tabcontent"+i);
                    tabLinks.forEach(function(el) {
                    el.addEventListener("click", openTabs);
                    });


                    function openTabs(el) {
                        var btnTarget = el.currentTarget;
                        var menu = btnTarget.dataset.menu;
                        console.log(btnTarget)
                        var tabLink = document.querySelectorAll(".tablinks" + i);
                        var tabCont = document.querySelectorAll(".tabcontent"+i);

                        tabCont.forEach(function(el) {
                            el.classList.remove("active");
                        });

                        tabLink.forEach(function(el) {
                            el.classList.remove("active");
                        });

                        document.querySelector("#" + menu).classList.add("active");

                        btnTarget.classList.add("active");
                    }
                }
            </script>
        @endsection

