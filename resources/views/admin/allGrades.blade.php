@extends('template')
@section("content")

    <div class="PageTitle-mt pageTitle">
        <h2>Les Grades</h2>
    </div>

    <div class="allContent">
        <h4>Tous les grades</h4>
        @foreach($allGrades as $grade)
            {{$grade->label}}
            {{$grade->description}}
        @endforeach
    </div>


    <!--form ajouter grade-->

    <div class="formAdmin">
        <h3 class="titleCenter">Créer un grade</h3>
        <form method="post" action="{{route('storeGrade')}}" >
            @csrf
            <div class="formSection">
                <label for="faction">Faction</label>
                <select  id="faction" name="faction">
                    <option></option>
                    @foreach($allFaction as $faction)
                        <option value="{{$faction->factions_id}}"> {{$faction->factions_label}} </option>
                    @endforeach
                </select >
            </div>
            <div class="formSection">
                <label for="compagnie"> compagnie</label>
                <select id="compagnie">
                    <option></option>
                    @foreach($allCompagnie as $compagnie)
                        <option value="{{$compagnie->id_compagnie_grades}}">{{$compagnie->label}}</option>
                    @endforeach
                </select>
            </div>

            <div class="formSection">
                <label for="faction"> personnage</label>
                <select  id="caracter" name="cracter">
                    <option></option>
                    @foreach($allCaracter as $Caracter)
                        <option value="{{$Caracter->caracters_id}}"> {{$Caracter->caracters_name}}  {{$Caracter->caracters_forename}}</option>
                    @endforeach
                </select >
            </div>

            <div class="formSection">
                <label for="label">nom</label>
                <input type="text" id="label" name="label">
            </div>

            <div class="formSection">
                <label for="description">description</label>
                <textarea type="text" id="description" name="description"></textarea>
            </div>

            <button class="btnPrimary" type="submit">Créer le grade</button>
        </form>
    </div>
@endsection
