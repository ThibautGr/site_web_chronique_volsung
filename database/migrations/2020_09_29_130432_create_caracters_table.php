<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCaractersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create("caracters",function (Blueprint $table){
           $table->id("caracters_id");
           $table->string("caracters_name"); //
           $table->string("caracters_forname")->nullable(); //
           $table->string("caracters_age");//
           $table->text("caracters_img_location");//
           $table->string("caracters_sexe");//
           $table->text("caracters_back_ground");//
           $table->boolean("caracters_is_alive")->default(TRUE);//
           $table->boolean("graded")->default(FALSE);//
            $table->dateTime("updated_at");
            $table->dateTime("created_at");
           $table->unsignedBigInteger('caracters_users_id');//
           $table->foreign('caracters_users_id')
               ->references("id")
               ->on("users")
               ->onDelete("restrict")
               ->onUpdate('restrict');

           $table->unsignedBigInteger('caracters_races_id');//
           $table->foreign('caracters_races_id')
           ->references("races_id")
           ->on("races")
           ->onDelete("restrict")
           ->onUpdate('restrict');
       });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('caracters');
    }
}
