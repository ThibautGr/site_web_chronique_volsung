<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInscriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inscription', function (Blueprint $table) {
            $table->id('inscription_id');
            $table->unsignedBigInteger('inscription_id_user');
            $table->foreign('inscription_id_user')
                ->references('id')
                ->on('users')
                ->onDelete("restrict")
                ->onUpdate('restrict');
            $table->boolean('inscription_is_confirm')->default(false);
            $table->string('inscription_doc_identity_location');
            $table->string('inscription_doc_rigth_img_location');
            $table->string('inscription_doc_parental_autorisation_location')->nullable();
            $table->string('inscription_doc_file_sanitary_location');
            $table->string('inscription_doc_discharge_responbility');

            $table->unsignedBigInteger('inscription_event_id');
            $table->foreign("inscription_event_id")
                ->references('event_id')
                ->on('events')
                ->onDelete("restrict")
                ->onUpdate('restrict');


            $table->integer('inscription_faction_array'); # list of number for each faction
            $table->dateTime("updated_at");
            $table->dateTime("created_at");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inscription');
    }
}
