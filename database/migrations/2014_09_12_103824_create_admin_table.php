<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin', function (Blueprint $table) {
            $table->id("id_level_admin"); // level admin
            $table->timestamps();
            $table->text("labelLevelAdmin");
            $table->text("description");

        });
    }
    /**
     * level admin (beta) :
     * level 1 : masterweb
     * level 2 : Editor of content (modify content of race, classe, faction)
     * level 3 : Editor of User (can modify user and content of them and validate him join of event)
     */

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin');
    }
}
