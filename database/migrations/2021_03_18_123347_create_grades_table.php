<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grades', function (Blueprint $table) {
            $table->id();
            $table->string('label');
            $table->text("description");

            $table->unsignedBigInteger('id_faction_grades');
            $table->foreign("id_faction_grades")
                ->references('factions_id')
                ->on('factions')
                ->onDelete("cascade")
                ->onUpdate("cascade");

            $table->unsignedBigInteger('id_character_grades')->nullable();
            $table->foreign("id_character_grades")
                ->references("caracters_id")
                ->on("caracters")
                ->onDelete("restrict")
                ->onUpdate("restrict");

            $table->unsignedBigInteger('id_compagnie_grades')->nullable();
            $table->foreign("id_compagnie_grades")
                ->references("compagnies_id")
                ->on("compagnies")
                ->onDelete("restrict")
                ->onUpdate("restrict");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grades');
    }
}
