<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompagniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compagnies', function (Blueprint $table) {
            $table->id("compagnies_id");
            $table->string('label');
            $table->text('description');
            $table->string('symboleUrl')->nullable();
            $table->unsignedBigInteger('compagnies_factions_id');//
            $table->foreign('compagnies_factions_id')
                ->references("factions_id")
                ->on("factions")
                ->onDelete("restrict")
                ->onUpdate('restrict');
            $table->dateTime("updated_at");
            $table->dateTime("created_at");
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compagnies');
    }
}
