<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classes', function (Blueprint $table) {
            $table->id("classes_id");
            $table->string('label');
            $table->text('description');
            $table->unsignedBigInteger('classes_factions_id');//
            $table->foreign('classes_factions_id')
                ->references("factions_id")
                ->on("factions")
                ->onDelete("restrict")
                ->onUpdate('restrict');
            $table->dateTime("updated_at");
            $table->dateTime("created_at");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classes');
    }
}
