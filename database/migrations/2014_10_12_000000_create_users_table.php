<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('forname');
            $table->integer('age');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->boolean('admin')->default(FALSE);

            $table->unsignedBigInteger("users_id_level_admin")->nullable();
            $table->foreign("users_id_level_admin")->nullable()
                ->references("id_level_admin")
                ->on("admin")
                ->onDelete("restrict")
                ->onUpdate('restrict');

            $table->rememberToken();
            $table->foreignId('current_team_id')->nullable();
            $table->text('profile_photo_path')->nullable();
            $table->string('doc_identity_location')->nullable();;
            $table->string('doc_rigth_img_location')->nullable();;
            $table->string('doc_file_sanitary_location')->nullable();
            $table->string('doc_discharge_responbility')->nullable();
            $table->timestamps();
        });
    }
/*            $table->id('users_id');
            $table->string('users_name');


            $table->string('users_email')->unique();
            $table->text('users_profil_img_location')->nullable();;
            $table->timestamp('users_email_verified_at')->nullable();
            $table->string('users_password');

            $table->string('users_doc_identity_location')->nullable();;
            $table->string('users_doc_rigth_img_location')->nullable();;
            $table->string('users_doc_file_sanitary_location')->nullable();;
            $table->string('users_doc_discharge_responbility')->nullable();;

            $table->unsignedBigInteger('Users_caractere_id')->nullable();;

            $table->foreign('Users_caractere_id')
                ->references("Caracters_id")
                ->on("Caracters")
                ->onDelete("restrict")
                ->onUpdate('restrict');*/
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
