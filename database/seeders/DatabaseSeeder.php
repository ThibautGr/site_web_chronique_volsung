<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::create(
       [
           'name' => 'nono',
           'forname'=>'Ornno',
           "age"=>'55',
           'email' => 'thibaut.gerin@hotmail.fr',
           'profile_photo_path'=>'/img/imgIconeUser/user_type_un.jpg',
           'password' => bcrypt('poulet'),
           'admin' => 1,
           'users_id_level_admin'=>1
       ] );
        \App\Models\User::create(
        [
            'name' => 'Eliokan',
            'forname'=>'Ornn',
            "age"=>'18',
            'email' => 'rio.gerin@hotmail.fr',
            'profile_photo_path'=>'/img/imgIconeUser/user_type_un.jpg',
            'password' => bcrypt('poulet'),
            'admin' => 0,
            'users_id_level_admin'=>0
        ]);
        \App\Models\User::create ( [
                'name' => 'Eliot',
                'forname'=>'Ornn',
                "age"=>'18',
                'email' => 'ro.gerin@hotmail.fr',
                'profile_photo_path'=>'/img/imgIconeUser/user_type_un.jpg',
                'password' => bcrypt('poulet'),
                'admin' => 0,
                'users_id_level_admin'=>0
            ]);

        \App\Models\Races::create([
            'label'=>'Aelf Idaris',
            'logevity'=>320,
            'description' =>'Aelf enfanter dans douleur de Adar'
        ]);
        \App\Models\Races::create([
            'label'=>'Aelf Lumis',
            'logevity'=>350,
            'description' =>'Aelf enfanter dans joie de Rodhan'
        ]);
        \App\Models\Admin::create([
            'labelLevelAdmin'=>'Webmaster',
            'description'=>'big boss qui a touts les droits'
        ]);
        \App\Models\Admin::create([
            'labelLevelAdmin'=>'Editor',
            'description'=>'Un chef qui peu écrires des choses'
        ]);

        \App\Models\Factions::create([
            'factions_label'=>"L'alliance du Dragon !",
            'factions_description'=>'Créers par un vieux monsieur il y a pas si longtemps'
        ]);
        \App\Models\Factions::create([
            'factions_label'=>"L’Empire d’Hiparzgul",
            'factions_description'=>'Crée il y a longtemps et ils son nombreux puissant et pas gentils '
        ]);
        \App\Models\Factions::create([
            'factions_label'=>"Les Légions d’Adroth",
            'factions_description'=>'Alors là, on une belle brochette de bio bobo qui font pas de cadeau'
        ]);
        \App\Models\Factions::create([
            'factions_label'=>"El famoso village d'albrcik",
            'factions_description'=>"Un petit village qui resiste encore et toujours à l'envahisseur"
        ]);

        \App\Models\Classes::create([
            'label'=>"chasseur",
            'description'=>"Le chasseur est la classe spé",
            'classes_factions_id'=>3
        ]);

        \App\Models\Classes::create([
            'label'=>"épéiste",
            'description'=>"Le chasseur est la classe spé",
            'classes_factions_id'=>2
        ]);
        \App\Models\Classes::create([
            'label'=>"Lancier",
            'description'=>"un petit gars avec une lance",
            'classes_factions_id'=>1
        ]);
        \App\Models\Classes::create([
            'label'=>"Lancier",
            'description'=>"un petit gars avec une lance",
            'classes_factions_id'=>1
        ]);
        \App\Models\Compagnies::create([
            'label'=>"Les santinelles du dragon",
            'symboleUrl'=>'img/compagnieLogo/compagniedefaut.png',
            'description'=>"des petite gars qui véners un dragon",
            'compagnies_factions_id'=>1
        ]);
        \App\Models\Compagnies::create([
            'label'=>"Les lions de vira",
            'symboleUrl'=>'img/compagnieLogo/compagniedefaut.png',
            'description'=>"des petite gars qui véners un lion",
            'compagnies_factions_id'=>3
        ]);
        \App\Models\Compagnies::create([
            'label'=>"Les lions de vira",
            'symboleUrl'=>'img/compagnieLogo/compagniedefaut.png',
            'description'=>"des petite gars qui véners un lion",
            'compagnies_factions_id'=>3
        ]);
        \App\Models\Compagnies::create([
            'label'=>"NAXIIR",
            'symboleUrl'=>'img/compagnieLogo/compagniedefaut.png',
            'description'=>"des petite gars qui véners un lion",
            'compagnies_factions_id'=>2
        ]);
        \App\Models\Compagnies::create([
            'label'=>"Les miliciens",
            'symboleUrl'=>'img/compagnieLogo/compagniedefaut.png',
            'description'=>"des petite gars qui véners un lion",
            'compagnies_factions_id'=>4
        ]);
        \App\Models\Events::create([
            'events_label'=>'Les chroniques de volsung - édition IX',
            'events_description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer dictum tincidunt risus, vitae molestie arcu gravida non. Pellentesque non pellentesque lectus. Integer tempus malesuada auctor. Nam eu ullamcorper velit. Phasellus elementum tincidunt metus vel iaculis. Pellentesque malesuada laoreet dictum. Fusce a purus et neque porttitor iaculis eget ac nisi. Phasellus posuere maximus sapien, at pellentesque urna eleifend vel. Ut id risus metus. Donec id dapibus lacus.',
            'events_start'=>'2021-08-05',
            'events_end'=>'2021-08-09',
            'events_price'=>'70'
        ]);
        \App\Models\Grades::create([
           'label'=>"général",
            "description"=>"l'aliance du dragon",
            "id_faction_grades"=> 1 ,
            "id_character_grades"=>1,
        ]);
        \App\Models\Grades::create([
            'label'=>"Lieutenant ",
            "description"=>"sentiennelle du dragon",
            "id_faction_grades"=> 1 ,
            "id_character_grades"=>2,
            "id_compagnie_grades"=>1,
        ]);
        \App\Models\Caracters::create([
            'caracters_name'=>'Elios',
            'caracters_forname'=>'poulet',
            'caracters_age'=>25,
            'caracters_img_location'=>'/img/imgIconeCaracters/user_type_un.jpg',
            'caracters_sexe'=>'Masculin',
            'caracters_back_ground'=>'lorem impsum impsumimpsumimpsumimpsum impsum',
            'caracters_is_alive' => true,
            'graded'=>true,
            'caracters_users_id'=>1,
            'caracters_races_id'=>1
        ]);
        \App\Models\Caracters::create([
            'caracters_name'=>'tes',
            'caracters_forname'=>'tes',
            'caracters_age'=>25,
            'caracters_img_location'=>'/img/imgIconeCaracters/user_type_un.jpg',
            'caracters_sexe'=>'Masculin',
            'caracters_back_ground'=>'lorem impsum impsumimpsumimpsumimpsum impsum',
            'caracters_is_alive' => true,
            'graded'=>true,
            'caracters_users_id'=>1,
            'caracters_races_id'=>1
        ]);
        \App\Models\Caracters::create([
            'caracters_name'=>'tes2',
            'caracters_forname'=>'tes32',
            'caracters_age'=>25,
            'caracters_img_location'=>'/img/imgIconeCaracters/user_type_un.jpg',
            'caracters_sexe'=>'poulet',
            'caracters_back_ground'=>'lorem impsum impsumimpsumimpsumimpsum impsum',
            'caracters_is_alive' => true,
            'graded'=>false,
            'caracters_users_id'=>1,
            'caracters_races_id'=>1
        ]);
        /*
         *  $table->string('events_label');
            $table->text('events_description');
            $table->date('events_start');
            $table->date("events_end");
            $table->integer("events_price");
         */
    }
}


