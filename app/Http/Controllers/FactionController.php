<?php

namespace App\Http\Controllers;

use App\Models\Factions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FactionController extends Controller
{
    public function secureAdmin(){
        $target = Auth::user()->admin ;
        if ($target == 1) {
            return true;
        }
        else{
            return redirect()->back();
        }
    }
    public function getAllFaction(){
        $allFactions = Factions::all();

        if ($this->secureAdmin()){
            return view('admin/allfactions',compact('allFactions'));
        }
    }

    public function getUpdater($id){
        $factionToUpdate = DB::table('factions')->where('factions_id',$id)->get();
        return view('admin/factionUpdater',compact("factionToUpdate"));
    }

    public function storeUpdateFaction(Request $request)
    {

        //$carcter = Caracters::find($request->caracters_id);

        $targetToUpdate =Factions::find($request->factions_id);

        /*
            $profile_photo_path = '/img/imgIconeUser/'.time().'.'.$userRequest->profile_photo_path->extension();
            $userRequest->profile_photo_path->move(public_path('/img/imgIconeUser'), $profile_photo_path);
         */


        if (isset($request->factions_symbole)) {
            $factions_symbole = '/img/symboleFaction/'.$request->factions_label.time().'.'.$request->factions_symbole->extension();
            $request->factions_symbole->move(public_path('/img/symboleFaction'),$factions_symbole);

            $targetToUpdate->factions_symbole = $factions_symbole;

        }
        $targetToUpdate->factions_label = $request->factions_label;
        $targetToUpdate->factions_description = $request->factions_description;
        $targetToUpdate->save();

        return redirect()->back();

    }
}
