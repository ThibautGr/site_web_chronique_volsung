<?php

namespace App\Http\Controllers;

use App\Models\Grades;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Caracters;
use Illuminate\Support\Facades\Auth;
use App\Models\Classes;
use App\Models\Races;
use App\services\RacesService;
use Illuminate\Support\Facades\DB;
use App\services\GradeAdminstrationService;
class ControllerCaracters extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }

    public function createCracterAndShow(RacesService $RacesService){
        $allRaces = $RacesService->getAllRace();
        $allClasses = Classes::all();
        $target = Auth::id();
        //SELECT * FROM caracters WHERE caracters_users_id = 1
        $caracteresUsers = DB::table("caracters")->where('caracters_users_id',$target)->get();
            return view('profile/caractersZone',compact('allRaces','allClasses', 'caracteresUsers'));
    }


    public function storeCracter(Request $request){
        $user = Auth::id();

        $rules = [
            'caracters_name'=>'required|min:2|max:50|alpha_dash',
            "caracters_forname"=>'required|min:2|max:50|alpha_dash',
            "caracters_age"=>"int",
            "caracters_img_location"=>"image|dimensions:min_width=100,min_height=200",
            "caracters_sexe"=>"max:50",
            "caracters_back_ground"=>'string|regex:^[a-zA-Z0-9 "!?.-]+$^|',
            "caracters_races_id"=>"required"
        ];
        $request->validate($rules);

        if($request->hasfile('caracters_img_location')){
            $caracters_img_location = '/img/imgIconeCaracters/'.time().'.'.$request->caracters_img_location->extension();
            $request->caracters_img_location->move(public_path('/img/imgIconeCaracters'), $caracters_img_location);
            $createCaracters = new Caracters();
            $createCaracters->caracters_name = $request->caracters_name;
            $createCaracters->caracters_forname = $request->caracters_forname;
            $createCaracters->caracters_age = $request->caracters_age;
            $createCaracters->caracters_sexe = $request->caracters_sexe;
            $createCaracters->caracters_back_ground = $request->caracters_back_ground;
            $createCaracters->caracters_races_id = $request->caracters_races_id;
            $createCaracters->caracters_users_id = $user;
            $createCaracters->caracters_img_location = $caracters_img_location;
            $createCaracters->save();
        }
       else if(empty($request->imgIconeCaracters)){
            $createCaracters = new Caracters();
            $createCaracters->caracters_name = $request->caracters_name;
            $createCaracters->caracters_forname = $request->caracters_forname;
            $createCaracters->caracters_age = $request->caracters_age;
            $createCaracters->caracters_sexe = $request->caracters_sexe;
            $createCaracters->caracters_back_ground = $request->caracters_back_ground;
            $createCaracters->caracters_races_id = $request->caracters_races_id;
           $createCaracters->caracters_users_id = $user;
            $createCaracters->caracters_img_location ='/img/imgIconeCaracters/user_type_un.jpg';
            $createCaracters->save();
        }
            return redirect()->back();
    }

    public function destroy($id)
    {
        $user = Caracters::where('caracters_id', $id)->firstorfail()->delete();
        return redirect()->route('MesPerso');
    }

    //get id user connecter
    //get all field in caractere selected by id

    public function getUpdateCara($id){
        $target = Auth::id();
        $targetCara = DB::table("caracters")->where('caracters_users_id',$target )->where('caracters_id',$id)->get();
        $raceLavbel = DB::table("races")->where('races_id',$targetCara[0]->caracters_races_id)->get();

            return  view("profile/caractereUpdate", compact("targetCara","raceLavbel"));
    }
    public function storeUpdateCara(Request $request){

        $carcter = Caracters::find($request->caracters_id);

        $carcter->caracters_name =$request->caracters_name;
        $carcter->caracters_forname = $request->caracters_forname;
        $carcter->caracters_age = $request->caracters_age;
        $carcter->caracters_back_ground = $request->caracters_back_ground;
        $carcter->save();


        return redirect()->route('MesPerso');
    }

    /**
     * show all characters
     * All charcters owner
     *All
     */

    public function getAllCharacters(){
        $allCharacters = Caracters::all();
        $owners = DB::table('users')->where('id' ,$allCharacters[0]->caracters_users_id)->get();
        $allGrade = Grades::all();

        $caractereGraded = DB::table('grades')
            ->join('caracters','grades.id_character_grades','=','caracters.caracters_id')
            ->select('grades.*','caracters.*')
            ->get();

        $nonGradedCracter = Caracters::all()->where('graded','=',FALSE);

        //ddd($caractereUnGraded);
        return view('admin.allCracters',compact('allCharacters','owners','allGrade','caractereGraded','nonGradedCracter'));
    }

    /*
 $flight = Flight::find(1);

$flight->name = 'Paris to London';

$flight->save();
     */
}
