<?php

namespace App\Http\Controllers;
use App\Models\Factions;
use App\Models\Classes;
use App\Models\Races;
use App\Models\Compagnies;
use App\Models\Events;
class NavbarController extends Controller {

    public function association(){
        return view("pages/association");
    }

    public function events(){
        $allEvents = Events::all();
        return view("pages/events", compact("allEvents"));
    }

    public function wiki(){
        $allFactions = Factions::all();
        $allClasses = Classes::all();
        $allRaces = Races::all();
        $allCompagnies = Compagnies::all();
        $allContent = [$allFactions, $allClasses, $allRaces, $allCompagnies];
        return view("pages/wiki", compact("allContent"));
    }


}



