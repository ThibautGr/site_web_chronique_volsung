<?php


namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\Admin;
use App\Models\Caracters;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function __construct()
    {
    }
    public function create(){
        return view('pages/inscription');
    }
    /*
     * fonction de sauvegarde utilise la class User pour crée un nouveau user dans la BDD
     * */
    public function store(UserRequest $userRequest)
    {
        $userRequest->validate($userRequest->rules());
        if($userRequest->hasfile('profile_photo_path'))
        {
            $profile_photo_path = '/img/imgIconeUser/'.time().'.'.$userRequest->profile_photo_path->extension();
            $userRequest->profile_photo_path->move(public_path('/img/imgIconeUser'), $profile_photo_path);
            $createUser = new User;
            $createUser->name = $userRequest->name;
            $createUser->forname = $userRequest->forname;
            $createUser->age=$userRequest->age;
            $createUser->profile_photo_path=$profile_photo_path;
            $createUser->password=Hash::make($userRequest['password']);
            $createUser->email=$userRequest->email;
            $createUser->save();
        }
        else if(empty($userRequest->profile_photo_path))
        {
            $createUser = new User;
            $createUser->name = $userRequest->name;
            $createUser->forname = $userRequest->forname;
            $createUser->age=$userRequest->age;
            $createUser->profile_photo_path= '/img/imgIconeUser/user_type_un.jpg';
            $createUser->password=Hash::make($userRequest['password']);
            $createUser->email=$userRequest->email;
            $createUser->save();
        }

        return back()->with('success', "vous êtes bien inscrit !");
    }
    //hydrat the view alluser in admin pannel
    public function getAllUser(){
        $allUsers = User::all();
        //$allAdmin = User::with('admin')->get();
        $allAdmin = Admin::all();
        return view("admin/allUsers",compact("allUsers","allAdmin" ));
    }
    //hydrat the view user in admin pannel
    public function getUser($id){
        if (Auth::user()->admin == 1 ){
            $user = User::where('id',$id)->get();
            $allAdminLabel = Admin::all();
            $caracters = Caracters::where('caracters_users_id',$id)->get();
            foreach ($user as $datauser){
                if($datauser->admin != 0)
                {
                    $adminlabel = Admin::where('id_level_admin',$datauser->users_id_level_admin)->get();
                }
            }
            if(!empty($adminlabel)){
                return view("admin/user",compact("user","caracters",'adminlabel',"allAdminLabel"));
            }
            else{
                return view("admin/user",compact("user","caracters","allAdminLabel"));
            }
        }
        else{
            return redirect()->back();
        }
    }
    //gestionnaire des admins

/*
'users_name'
'users_forname'
'users_age'
'
email'
'users_admin'
'profile_photo_path'
'password'
'users_doc_identity_location'
'users_doc_rigth_img_location'
'users_doc_file_sanitary_location'
'users_doc_discharge_responbility'
*/

}
