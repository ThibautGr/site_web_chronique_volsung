<?php


namespace App\Http\Controllers\Auth;

use illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;
class ImageController
{

    public function update_profile_photo(Request $request){
        $user = User::find(Auth::id());
        $rules=
            [
                'profile_photo_path' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ];
        $request->validate($rules);
        if(!empty($request->profile_photo_path)){
            $profile_photo_path = '/img/imgIconeUser/'.time().'.'.$request->profile_photo_path->extension();
            $request->profile_photo_path->move(public_path('/img/imgIconeUser'), $profile_photo_path);
            $profilUpdate = $user;
            $profilUpdate -> profile_photo_path = $profile_photo_path;
            $profilUpdate->save();
        }
        return redirect()->back();
    }
}
