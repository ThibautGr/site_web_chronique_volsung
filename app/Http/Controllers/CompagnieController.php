<?php

namespace App\Http\Controllers;

use App\Models\Compagnies;
use App\Models\Factions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CompagnieController extends Controller
{
    public function getAllCompagnie(){
       $allCompagnies = Compagnies::all();
       $allFactions = Factions::all();

        return view('admin.allcompagnies', compact('allCompagnies','allFactions'));
    }

    public function getUpdater($id){
    $compagnieToUpdate = DB::table('compagnies')->where('compagnies_id',$id)->get();
    return(view('admin.compagnieUpdater',compact('compagnieToUpdate')));
    }

    public function storeUpdateCompagnie(Request $request){
        $targetToUpdate = Compagnies::find($request->compagnies_id);

        if (isset($request->symboleUrl)){
            $symboleUrl = '/img/symboleCompagnies/'.$request->label.time().'.'.$request->symboleUrl->extension();
            $request->symboleUrl->move(public_path('img/symboleCompagnies'),$symboleUrl);

            $targetToUpdate->symboleUrl = $symboleUrl;
        }

        $targetToUpdate->label = $request->label;
        $targetToUpdate->description = $request->description;
        $targetToUpdate->save();
        return redirect()->back();
    }


    public function storeNewCompagine(Request $request){
        $newCompagnie = new Compagnies();
    if ( $request->symboleUrl == !null){
        $symboleUrl = '/img/compagnieLogo/'.$request->label.time().'.'.$request->symboleUrl->extension();
        $request->symboleUrl->move(public_path('img/compagnieLogo'),$symboleUrl);

         $newCompagnie->symboleUrl = $symboleUrl;
     }
    else {
        $request->symboleUrl = 'img/compagnieLogo/compagniedefaut.png';
    }

      $newCompagnie->label = $request->label;
      $newCompagnie->description = $request->description;
      $newCompagnie->compagnies_factions_id  = $request->compagnies_factions_id;

      $newCompagnie->save();

      return redirect()->back();

    }
}
