<?php

namespace App\Http\Controllers;

use App\Models\Caracters;
use App\Models\Compagnies;
use App\Models\Factions;
use App\Models\Grades;
use Illuminate\Http\Request;

class GradeController extends Controller
{

    /*TODO
     * Updater by id
     * Assign to this caracter by screen of all caracter
    */
    public function getAllGrades(){
        $allGrades = Grades::all();
        $allFaction = Factions::all();
        $allCompagnie = Compagnies::all();
        $allCaracter = Caracters::all();
        return view('admin.allGrades', compact('allGrades', 'allFaction', 'allCompagnie','allCaracter'));
    }

    public function storeGrade(Request $request){

        //penser a définir des règles de validation


        $createGrade = new Grades();
        if (isset($request->compagnie)){
            $createGrade->id_compagnie_grades = $request->compagnie;
        }
        if (isset($request->character)){
            $createGrade->id_character_grades = $request->character;
        }
        $createGrade->label = $request->label;
        $createGrade->description = $request->description;
        $createGrade->id_faction_grades = $request->faction;
        $createGrade->save();

        redirect()->back();
    }
}
