<?php

namespace App\Http\Controllers;

use App\Models\Races;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function homeAdmin() {
        $target = Auth::user()->admin ;
        if ($target == true) {
        return view('admin/adminHome');
        }
        else{
            return redirect()->back();
        }
    }

    public function createRacesAdmin(){
        $target = Auth::user()->admin ;
        $allraces = Races::all();
        if ($target == true) {
            return view('admin/createRaces',compact('allraces'));
        }
        else{
            return redirect()->back();
        }
    }

    public function storeRacesAdmin(Request $request){
        $rules = [
            "label"=> 'required|min:2|max:50|alpha_dash',
            "logevity"=>"int",
            "description"=>'string|regex:^[a-zA-Z0-9 "!?.-]+$^|'
        ];

       if ($request->validate($rules)) {
          $createRace = new Races();
          $createRace->label = $request->label;
          $createRace->logevity = $request->logevity;
          $createRace->description = $request->description;
          $createRace->save();
       }
        return redirect()->back();
    }


    public function updateAdmin(Request $request) {
        if(Auth::user()->users_id_level_admin == 1)
        {
            $userToChange = User::find($request->user_admin_change_id);
            $rules = [
                "admin" => 'required|int'
            ];
            if ($request->validate($rules)){
                //if user demoted to normal user
                if($request->admin == 0){
                    $userToChange->admin=$request->admin;
                    $userToChange->users_id_level_admin = 0;
                    $userToChange->save();
                    return redirect()->back();
                }
                //if user promoted to admin rank
                else if ($request->admin == 1 && !empty($request->label)){
                    $userToChange->admin=$request->admin;
                    $userToChange->users_id_level_admin=$request->label;
                    $userToChange->save();
                    return redirect()->back();
                }
                //if error
                else{
                    return "somthing wrong happen";
                }
            }
        }
        else
        {
            return redirect()->back();
        }

    }
}
