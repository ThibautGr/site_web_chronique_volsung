<?php

namespace App\Http\Controllers;

use App\Models\Events;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\services\EventsService;

class EventController extends Controller
{
    public function secureAdmin(){
        $target = Auth::user()->admin ;
        if ($target == 1) {
            return true;
        }
        else{
            return redirect()->back();
        }
    }

    public function createEventsAdmin(){
        $target = Auth::user()->admin ;
        $allevents = Events::all();
        if ($target == true) {
            return view('admin/createEvents',compact('allevents'));
        }
        else{
            return redirect()->back();
        }
    }

    public function getAllEvents(){
        $allFactions = Events::all();

        if ($this->secureAdmin()){
            return view('events',compact('allEvents'));
        }
    }

    public function getUpdater($id){
        $factionToUpdate = DB::table('events')->where('events_id',$id)->get();
        return view('admin/eventsUpdater',compact("eventToUpdate"));
    }

    public function storeUpdateEvent(Request $request)
    {
        $targetToUpdate =Events::find($request->events_id);

        $targetToUpdate->events_label = $request->events_label;
        $targetToUpdate->events_description = $request->events_description;
        $targetToUpdate->events_price = $request->events_price;
        $targetToUpdate->events_start = $request->events_start;
        $targetToUpdate->events_end = $request->events_end;
        $targetToUpdate->save();

        return redirect()->back();

    }

    public function storeEvent(Request $request){
        $rules = [
            "events_label"=>'required|min:2|max:150|',
            "events_description"=>'required|string|regex:^[a-zA-Z0-9 "!?.-]+$^|',
            "events_price"=>"required|int",
            "events_start"=>"required|string",
            "events_end"=>"required|string"
        ];
        $request->validate($rules);
            $createEvent = new Events();
            $createEvent->events_label = $request->events_label;
            $createEvent->events_description = $request->events_description;
            $createEvent->events_price = $request->events_price;
            $createEvent->events_start = $request->events_start;
            $createEvent->events_end = $request->events_end;
            $createEvent->save();

            return redirect()->back();
    }

    public function destroy($id)
    {
        Events::where('events_id', $id)->firstorfail()->delete();
        return redirect()->route('events');
    }

    //get id user connecter
    //get all field in caractere selected by id

    public function getUpdateEvent($id){
        $target = Auth::id();
        $targetEvent = DB::table("events")->where('events_id',$id)->get();
            return  view("admin/eventUpdate", compact("targetEvent"));
    }

}
