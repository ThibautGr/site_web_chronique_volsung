<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return True;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|min:2|max:50|alpha_dash',
            'forname'=>'required|min:2|max:50|alpha_dash',
            'age'=>'required|int',
            'email'=>'required|string|email|max:255|unique:users',
            'admin'=>'nullable|boolean',
            "profile_photo_path"=>'image|dimensions:min_width=100,min_height=200',
            'password'=>'required|string|min:6|confirmed'
        ];
    }
    public function messagesCreate()
    {
        return [
            'prenom.required'=> "le nom est requis"
        ];
    }
}
