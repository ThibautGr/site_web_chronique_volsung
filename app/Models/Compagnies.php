<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Compagnies extends Model
{
    use HasFactory;
    protected $primaryKey ="compagnies_id";
    protected $fillable = [
        "label",
        "description",
        "classes_factions_id"
    ];


}
