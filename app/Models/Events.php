<?php


namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Events extends Model
{

    protected $primaryKey ="Events_id";
    protected $fillable = [
        "events_label",
        "events_description",
        "events_price",
        "events_start",
        "events_end"
    ];
}
