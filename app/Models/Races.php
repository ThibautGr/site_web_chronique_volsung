<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Races extends Model
{
    use HasFactory;
    protected $table='races';
    protected $primaryKey ="races_id";
    protected $fillable = [
        "label",
        "logevity",
        "description"
    ];

/*
                $table->id("races_id");
                $table->string("label");
                $table->string("logevity");
                $table->text("description");
 */
}
