<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Caracters extends Model
{
    protected $primaryKey ="caracters_id";
    use HasFactory;
    //protected $table = "caracters";
    protected $fillable = [
        "caracters_name",
        "caracters_forname",
        "caracters_age",
        "caracters_img_location",
        "caracters_sexe",
        "caracters_back_ground",
        'caracters_races_id',
        "caracters_users_id"

    ];
    public function grades()
    {
        return $this->belongsTo(Grades::class,'id_character_grades','caracters_id');
    }

}
