<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $primaryKey ="id_level_admin";
    protected $table = "admin";
    use HasFactory;
    protected $fillable = [
        "labelLevelAdmin",
        "description"
    ];
    /**
     *
        $table->id("id_level_admin"); // level admin
        $table->timestamps();
        $table->text("labelLevelAdmin");
        $table->text("description");

     */
}
