<?php


namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Factions extends Model
{

    protected $primaryKey ="factions_id";
    protected $fillable = [
        "factions_label",
        "factions_description"
    ];
}
