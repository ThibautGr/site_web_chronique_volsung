<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Grades extends Model
{
    use HasFactory;

   public function caracters(){
       return $this->hasOne(Caracters::class,'caracters_id','id_character_grades');
   }
}
