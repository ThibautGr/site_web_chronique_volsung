<?php
use \App\Http\Controllers\AdminController;
use App\Http\Controllers\CompagnieController;
use App\Http\Controllers\ControllerCaracters;
use App\Http\Controllers\FactionController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\GradeController;
Route::prefix('/orga')->group(function (){

    //Route::get('mescaracters',[ControllerDeleteCaractere::class,'delleteCractere/{id}']);
    Route::middleware(['auth:sanctum','verified'])
        ->get('/Home',[AdminController::class,'homeAdmin'])->name('HomeAdmin');

// for orga webmaster admin Only
    Route::middleware(['auth:sanctum','verified'])
        ->get('/createRaces',[AdminController::class,'createRacesAdmin'])->name('createRacesAdmin');

    Route::post('/createRaces',[AdminController::class,"storeRacesAdmin"])->name("stroreRace");


//for all admin
    Route::middleware(['auth:sanctum','verified'])
        ->get('/allUsers',[UsersController::class,'getAllUser'])->name('allUser');

    Route::middleware(['auth:sanctum','verified'])
        ->get("/user/{id}",[UsersController::class,'getUser'])->name("user");

    Route::post('/upgradeAdmin',[AdminController::class,"updateAdmin"])->name("updateAdmin");

//service Faction
    Route::middleware(['auth:sanctum','verified'])
        ->get("/allfactions",[FactionController::class,'getAllFaction'])->name("allFaction");

    Route::middleware(['auth:sanctum','verified'])
        ->get("/allfactions/modif/{id}",[FactionController::class,'getUpdater'])->name("allFaction.updater");


    Route::post('/allfaction/modif/store',[FactionController::class,"storeUpdateFaction"])->name("storeFactionUpdate");

    //service compagnie
    Route::middleware(['auth:sanctum','verified'])
        ->get('/allcompagnies',[CompagnieController::class,'getAllCompagnie'])->name("allCompagnies");


    Route::middleware(['auth:sanctum','verified'])
        ->get("/allcompagnies/modif/{id}",[CompagnieController::class,'getUpdater'])->name("allCompagnies.updater");

    Route::post('/allcompagnies/modif/store',[CompagnieController::class,"storeUpdateCompagnie"])->name("storeCompagnieUpdate");

    Route::post('/allcompagnies/store',[CompagnieController::class,'storeNewCompagine'])->name("storeNewCompagine");

    // service events


    Route::post('/createEvents', [EventController::class, 'storeEvent'])->name("storeEvent");
    Route::middleware(['auth:sanctum','verified'])
        ->get('/createEvents',[EventController::class,'createEventsAdmin'])->name('createEventsAdmin');

    //service Grade

    Route::middleware(['auth:sanctum','verified'])
        ->get('/getallGrades',[GradeController::class,'getAllGrades'])->name('allGrades');

    Route::post('/getallGrades/store',[GradeController::class,"storeGrade"])->name("storeGrade");

    //serve characters
    Route::middleware(['auth:sanctum','verified'])
        ->get('/getallcharacters',[ControllerCaracters::class,'getAllCharacters'])->name('allCharacters');

    // Route::delete('/{id}', [EventController::class, 'destroy'])->name('event.destroy');
    // Route::middleware(['auth:sanctum', 'verified'])
    //     ->get('/modif/{id}', [EventController::class, "getUpdateEvent"])->name("getUpdateEvent");
    // Route::post('/modif', [EventController::class, "storeUpdateEvent"])->name("storeUpdateEvent");



});

