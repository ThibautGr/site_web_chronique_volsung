<?php

use App\Http\Controllers\ControllerCaracters;

Route::prefix('/mescaracters')->group(function () {
    Route::middleware(['auth:sanctum', 'verified'])
        ->get('/', [ControllerCaracters::class, 'createCracterAndShow'])->name("MesPerso");

    Route::post('/', [ControllerCaracters::class, 'storeCracter'])->name("storeCractere");

    Route::delete('/{id}', [ControllerCaracters::class, 'destroy'])->name('mescaracters.destroy');

    Route::middleware(['auth:sanctum', 'verified'])
        ->get('/modif/{id}', [ControllerCaracters::class, "getUpdateCara"])->name("mescaracters.update");
    Route::post('/modif', [ControllerCaracters::class, "storeUpdateCara"])->name("mescaracters.storeUpdate");
});
