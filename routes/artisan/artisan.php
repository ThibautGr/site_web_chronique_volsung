<?php
use \Illuminate\Support\Facades\Artisan;

/*
 * TODO delete this files before make the web site online !!!!!
 */
Route::prefix('/artisan')->group(function () {

    Route::get('/clear', function(){
        Artisan::call('cache:clear');
        Artisan::call('config:clear');
        Artisan::call('view:clear');
        Artisan::call('key:generate');

        return "all Cleared!";
    });

    Route::get('/migratefresh', function(){
        Artisan::call('migrate:refresh');

        return "migrate refresh !";
    });

    Route::get('/seed', function(){
        Artisan::call('db:seed');

        return "db seeded !";
    });


});

