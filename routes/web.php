<?php

use App\Http\Controllers\Auth\ImageController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AccueilController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\NavbarController;
use \App\Http\Controllers\ControllerDeleteCaractere;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', [AccueilController::class, 'accueil'])->name('accueil');
Route::get("inscription",[UsersController::class, 'create'])->name('inscription.create');
Route::post("inscription",[UsersController::class, 'store'])->name('inscription.store');

Route::get('association', [NavbarController::class, 'association'])->name('association');

Route::get('wiki', [NavbarController::class, 'wiki'])->name('wiki');
Route::get('events', [NavbarController::class, 'events'])->name('events');


include 'mescaracters/mescracter.php';

include 'admin/admin.php';

include 'artisan/artisan.php';



