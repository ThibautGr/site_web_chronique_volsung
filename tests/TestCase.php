<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    public function name_and_email_fields_are_required_for_saving_a_contact()
    {
        Livewire::test('contact-form')
            ->set('name', '')
            ->set('email', '')
            ->assertHasErrors(['name', 'email']);
    }
}
